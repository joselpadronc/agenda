<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cookie;

// Models
use App\Models\User;
use App\Models\Level;

class AuthController extends Controller
{
    public function login_view() {
        return view('auth.login');
    }

    public function login_auth(Request $request) {
        $user = User::getUserByUsername($request->get('user'));

        if(!empty($user)) {
            if(Hash::check($request->get('password'), $user->cla_usu)) {
                $userLevel = Level::select('*')->where('ide_car', '=', $user->niv_usu)->first();

                $request->session()->put([
                    'user_session' => [
                        'username' => $user->nom_usu,
                        'levelId' => $user->niv_usu,
                        'id' => $user->ide_usu,
                        'level' => $userLevel->des_car,
                    ],
                ]);

                return response()->json([
                    'success' => true,
                    'message' => 'Bienvenido',
                ]);

            }else {
                return response()->json([
                    'success' => false,
                    'message' => 'Contraseña incorrecta',
                ]);
            }
        }else {
            return response()->json([
                'success' => false,
                'message' => 'Usuario incorrecto',
            ]);
        }
    }

    public function logout(Request $request){
        $request->session()->flush();

        return redirect()->route('login');
    }
}
