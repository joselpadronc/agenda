<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    use HasFactory;

    // Base de datos y tabla que se va a usar
    protected $table = "saia.cargo";
    protected $primaryKey = 'ide_car';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'des_car',
        'sta_car'
    ];

}
