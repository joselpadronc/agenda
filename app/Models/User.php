<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    // Base de datos y tabla que se va a usar
    protected $table = "saia.usuarios";
    protected $primaryKey = 'ide_usu';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom_usu',
        'cla_usu',
        'niv_usu'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'cla_usu',
    ];

    public $timestamps = false;

    public static function getUserByUsername($username) {
        return self::select('*')
                ->where('nom_usu', '=', $username)
                ->where('sta_usu', '=', 1)
                ->first();
    }

    public function level() {
        return $this->hasOne(Level::class);
    }
}
