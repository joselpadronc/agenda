/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other entry modules.
(() => {
/*!*********************************************!*\
  !*** ./resources/js/views/createContact.js ***!
  \*********************************************/
var infoEmail = document.getElementById('InfoEmail');
var infoPhone = document.getElementById('InfoPhone');
var addEmailBtn = document.getElementById('addEmail');
var addPhoneBtn = document.getElementById('addPhone');

function createInputs(type1, type2, placeholder1, placeholder2, name1, name2, parent, button) {
  var newInputs = "\n    <input\n      class=\"form-control mb-2\"\n      type=\"".concat(type1, "\"\n      placeholder=\"").concat(placeholder1, "\"\n      name=\"").concat(name1, "[]\"\n      id=\"").concat(name1, "\"\n      required\n    >\n    <input\n      class=\"form-control mb-2\"\n      type=\"").concat(type2, "\"\n      placeholder=\"").concat(placeholder2, "\"\n      name=\"").concat(name2, "[]\"\n      id=\"").concat(name2, "\"\n      required\n    >\n  ");
  var div = document.createElement("div");
  div.className = "mt-3";
  div.innerHTML = newInputs;
  parent.insertBefore(div, button);
}

addEmailBtn.onclick = function () {
  createInputs("email", "text", "Otro correo", "Descripcion", "correoDir", "correoDes", infoEmail, addEmailBtn);
};

addPhoneBtn.onclick = function () {
  createInputs("text", "text", "Otro numero", "Descripcion", "telNro", "telDes", infoPhone, addPhoneBtn);
};
})();

// This entry need to be wrapped in an IIFE because it need to be isolated against other entry modules.
(() => {
/*!************************************!*\
  !*** ./resources/js/views/otro.js ***!
  \************************************/

})();

/******/ })()
;