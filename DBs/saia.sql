-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-07-2021 a las 17:20:24
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `saia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria`
--

CREATE TABLE `auditoria` (
  `ide_aud` int(6) NOT NULL,
  `fec_aud` date NOT NULL,
  `ipe_aud` varchar(20) NOT NULL,
  `nue_aud` text NOT NULL,
  `vie_aud` text NOT NULL,
  `acc_aud` varchar(60) NOT NULL,
  `usu_aud` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE `cargo` (
  `ide_car` int(6) NOT NULL,
  `des_car` varchar(60) NOT NULL,
  `sta_car` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`ide_car`, `des_car`, `sta_car`) VALUES
(1, 'Programadores', 1),
(2, 'Mantenimiento', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE `departamento` (
  `ide_dep` int(6) NOT NULL,
  `des_dep` varchar(60) NOT NULL,
  `ide_dpe` int(6) NOT NULL,
  `sta_dep` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`ide_dep`, `des_dep`, `ide_dpe`, `sta_dep`) VALUES
(1, 'Comunicaciones', 3, 1),
(2, 'Rescate', 3, 1),
(3, 'Ambulacias', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dependencia`
--

CREATE TABLE `dependencia` (
  `ide_dpe` int(6) NOT NULL,
  `des_dpe` varchar(60) NOT NULL,
  `sta_dpe` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `dependencia`
--

INSERT INTO `dependencia` (`ide_dpe`, `des_dpe`, `sta_dpe`) VALUES
(1, 'Servicios Medico', 1),
(2, 'Direccion', 1),
(3, 'Operaciones', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `ide_est` int(6) NOT NULL,
  `des_est` varchar(60) NOT NULL,
  `sta_est` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`ide_est`, `des_est`, `sta_est`) VALUES
(1, 'Tachira', 1),
(2, 'Merida', 1),
(3, 'Barinas', 1),
(4, 'Zulia', 1),
(5, 'Lara', 1),
(8, 'carabobo', 1),
(9, 'Bolivar', 1);

--
-- Disparadores `estado`
--
DELIMITER $$
CREATE TRIGGER `trigger_Update_estado` AFTER UPDATE ON `estado` FOR EACH ROW INSERT INTO auditoria(fec_aud,acc_aud, nue_aud, vie_aud,usu_aud,ipe_aud )
VALUES (NOW(),'Modificar',new.des_est,OLD.des_est,CURRENT_USER(),CURRENT_USER())
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funcionarios`
--

CREATE TABLE `funcionarios` (
  `ide_fun` int(6) NOT NULL,
  `ced_fun` varchar(20) NOT NULL,
  `nom_fun` varchar(60) NOT NULL,
  `ape_fun` varchar(60) NOT NULL,
  `dir_fun` text NOT NULL,
  `tel_fun` varchar(20) NOT NULL,
  `cel_fun` varchar(20) NOT NULL,
  `ide_par` int(6) NOT NULL,
  `sta_fun` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `funcionarios`
--

INSERT INTO `funcionarios` (`ide_fun`, `ced_fun`, `nom_fun`, `ape_fun`, `dir_fun`, `tel_fun`, `cel_fun`, `ide_par`, `sta_fun`) VALUES
(1, '1198496', 'JOSE GABRIEL', 'MARIN FERRER', 'Urb. Los Teques, Edif.27, Apto.00-01, Planta Baja', '412082 / 016-6766163', '', 1, 1),
(2, '1521845', 'ABRAHAM', 'BLANCO JAIMES', 'calle principal, Barrio el rio, casa N° 6-8', '0276-3472089', '', 1, 1),
(3, '1559758', 'JOSE NAPOLEON', 'CANCHICA SERVITA', 'Urb. Pirineos I Lote C Vda. 13 No.07', '558271', '', 1, 1),
(4, '2075538', 'LUIS OSCAR', 'RAMIREZ TORRES', 'Calle 01, No. 83, Urb. Sucre, S/C', '555311 (H) / 552061 ', '', 1, 1),
(5, '2137356', 'ADOLFREDO', 'RODRIGUEZ DELGADO', 'Lote B, Vereda 24, No.12,', '0276-3533525, 0276-3', '', 1, 1),
(6, '2550188', 'GERARDO JESUS', 'SÁNCHEZ GUERRERO', 'Calle El Rosal, No R-26', '0276-3564886', '', 1, 1),
(7, '2965609', 'HECTOR JOSE', 'CASTRO MIJARES', '', '', '', 1, 1),
(8, '2984609', 'RAFAEL ANTONIO', 'ROA TORRES', 'Av. Carabobo, No. 2080-A, S/C.', '420957', '', 1, 1),
(9, '3008458', 'HUGO ATILIO', 'GARCIA BUITRAGO', '.', '', '', 1, 1),
(10, '3054700', 'JOSE RAUL', 'CAMACHO FAGUNDEZ', 'Av. Tito Salas, No. 214', '0276-3477396', '', 1, 1),
(11, '3064829', 'LUIS ANDRES', 'CHACON', 'Residencias Capojud Torre Azul Apto 01-02 Colinas de Carabobo Sta Teresa', '', '04147251997', 1, 1),
(12, '3077358', 'JESUS MANUEL', 'CHACON', 'Rancherias via capacho, calle Bolivar, NÂ° 16-621', '', '', 1, 1),
(13, '3194111', 'RIGOBERTO', 'SANABRIA CASTRO', 'Urbanización Santa Rosa, Av. Mateo Manaure Nº 66 \"Nuestra Casita\"', '', '', 1, 1),
(14, '3223862', 'JOSE LUIS', 'ACOSTA GOMEZ', 'Pasaje Mucurita, No. 5-27, Barrio Obrero, S/C.', '3419494', '', 1, 1),
(15, '3310773', 'JOSE RAMON', 'ZAMBRANO SANCHEZ', 'Pasaje Yagual, No. 12-16, Puente Real.', '439963', '', 1, 1),
(16, '3402405', 'BERARDO ABIGAIL', 'TORRES RODRIGUEZ', '', '', '', 1, 1),
(17, '3621961', 'JULIO CESAR', 'ZAMBRANO GOMEZ', 'Carrera 5, No.5-48, Apto.7, Tariba', '942354 TARIBA / 4720', '', 1, 1),
(18, '3710751', 'HECTOR RUBEN', 'MONASTERIO', 'Carrera 7, No. 2-18, TÃ¡riba', '', '', 1, 1),
(19, '3793669', 'WILHELM ALFRED', 'JACZKO SCHER', 'Urb. Colinas de Pirineos, Av. 1, No. 117, Quinta Chinita, S/C', '568214 / 016 6761647', '', 1, 1),
(20, '3794001', 'ANTONIO JOSE', 'ROLDAN MENDOZA', 'Calle 1, No.17, Urb. Luisa Teresa Pacheco de Chacon, San Josecito IV', '', '', 1, 1),
(21, '3999744', 'LUIS EDUARDO', 'SANCHEZ YANEZ', 'VÃ­a Principal la Machiri, No. 65', '0414-5677966', '', 1, 1),
(22, '4092302', 'EMILIO ENRIQUE', 'DUQUE MENDEZ', 'Av. Pirineos, No. 0-98, S/C.', '0276-3556794', '', 1, 1),
(23, '4209223', 'JOSE ELIAS', 'VARELA DAZA', 'Calle 1, No.21, San Josecito IV.', '014-9778172', '', 1, 1),
(24, '4211390', 'FELIX ORLANDO', 'ZAMBRANO MORA', 'Av. 2 con Calle 5, Sta. Eduviges, No. 4-121, Tariba.', '942114', '', 1, 1),
(25, '4627073', 'FREDDY ORLANDO', 'LAGOS RAMIREZ', 'Calle 1, No.19, Urb. Luisa Teresa Pacheco de Chacon, San Josecito', '0276-7640180', '', 1, 1),
(26, '4629410', 'RAFAEL ANGEL', 'MORALES HERNANDEZ', 'Urb. Los Teques, Edf. 27, Apto. 03-03', '446933', '', 1, 1),
(27, '4976065', 'ANATOLIO', 'ORTEGA MOGOLLON', 'El Abejal, Entrada Ppal. Campo Deportivo, Palmira', '', '', 1, 1),
(28, '4992959', 'PEDRO EMIRO', 'GONZALEZ QUINTERO', 'Av. Ppal. de la Machirí, No. 59-41, S.C.', '411934 / 412545', '', 1, 1),
(29, '5021567', 'PABLO BERNARDO', 'DAZA PEREZ', 'Carrera 14 con Calle 3 Y 4, No.3-12, La Guacara, San Cristobal', '435433', '', 1, 1),
(30, '5022104', 'JOSE EMILIANO', 'MENDEZ LOPEZ', 'Pasaje Guasdualito, No. 14-26 Puente Real, S.C.', '0276-3410797', '', 1, 1),
(31, '5026269', 'TOMAS', 'RAMIREZ ROMERO', '.', '', '', 1, 1),
(32, '5026563', 'ANSELMO ENRIQUE', 'GRATEROL LINARES', '.', '', '', 1, 1),
(33, '5026993', 'PEDRO ALIRIO', 'RAMIREZ GUERRERO', 'Urb. Cesar Morales C., Calle 10, Sec. 2, No. 14, Palmar de la Cope', '464921', '', 1, 1),
(34, '5027587', 'FREDDY EUGENIO', 'COLMENARES GOMEZ', 'Finca La Esmeralda, Carretera Vieja, Sabaneta.', '461568', '', 1, 1),
(35, '5031427', 'LUCAS SAMUEL', 'GALVIS JAUREGUI', 'Calle 2, No. 03, Urb. Centenario, Sta. Ana del Tachira.', '67507', '', 1, 1),
(36, '5033076', 'JOSE MANUEL', 'VILLASMIL', 'Barrio La Mina, Vereda 1, No. 61, Los Alticos, Vega de Asa.', '640474', '', 1, 1),
(37, '5138158', 'FREDDY ANTONIO', 'FORTOUL', 'Vereda 16 No. 10, Pirineos II, S/C.', '561181', '', 1, 1),
(38, '5287768', 'CAMILO', 'CONTRERAS SIERRA', 'Calle 4, No.06, Urb. Luisa Teresa Pacheco de Chacon, San Josecito IV', '', '', 1, 1),
(39, '5326403', 'LUZ MARINA', 'VIVAS VILLAMIZAR', 'Carrera 21 Edf. Virute Piso 2 Apto. 5, Barrio Obrero', '554603', '', 1, 1),
(40, '5644880', 'PEDRO ELIAS', 'PERNIA CHACON', 'Calle 12 No. 23-72, Barrio Obrero S/C.', '551868', '', 1, 1),
(41, '5648012', 'RUBEN DARIO', 'MARQUEZ MENDEZ', '', '', '', 1, 1),
(42, '5650185', 'VICTOR MANUEL', 'ANTELIZ FERNANDEZ', 'Villa Olimpica, Edf. Los Caobos, Pso. 5, Apto. 54, Las Lomas', '', '', 1, 1),
(43, '5652997', 'EDWARD WILFREDO', 'MORALES MORENO', '', '', '', 1, 1),
(44, '5654508', 'SELIXA', 'CHACON', 'Carrera 13 No. 18-24, La Guayana', '', '', 1, 1),
(45, '5657594', 'MAGALY', 'ZAMBRANO DE MONTOYA', 'Barrio Ambrosio Plaza, Calle 2 Bis, No 2-9', '0276-3530895', '', 1, 1),
(46, '5660996', 'JORGE ENRIQUE', 'ALVAREZ VELAZCO', 'Barrio Sucre, Vereda 20 No. 2, S/C.', '0276-3552545', '0426-9747468', 1, 1),
(47, '5663796', 'GERARDO  ENRRIQUE', 'MENDEZ FIGUEROA', '.', '', '', 1, 1),
(48, '5664466', 'NELSON GERARDO', 'TORRES PORRAS', 'Vereda 4 casa Nro. 237', '3480322', '', 1, 1),
(49, '5668209', 'LUIS ALBERTO', 'MONCADA ROMERO', 'Barrio San Jose, Calle 5, No. 1-71, via Los Kioskos', '0276-3414259', '', 1, 1),
(50, '5672567', 'ALEXIS JOSE', 'RUIZ', 'Vereda 14, No. 3, Unidad Vecinal, La Concordia, S/C', '471520', '', 1, 1),
(51, '5676321', 'GERSON DARIO', 'VIVAS', 'Prolong. Carrera 3, No. 5-04, al lado de Mitsubishi, S.C.', '441042', '', 1, 1),
(52, '5677910', 'ABDEL ARTURO', 'RODRIGUEZ MARCIANI', 'Carrera 12, No. 13-46, Barrio Obrero, San CristÃ³bal.', '435207', '', 1, 1),
(53, '5683703', 'LUIS ANTONIO', 'SANCHEZ VIVAS', 'Carrera 14, No.15-15, Barrio San Carlos,', '0276-3439676', '', 1, 1),
(54, '5684822', 'JULIO ANTONIO', 'ROJAS ORTEGA', 'Av. Ferrero Tamayo, Resd. Friuli, Pso. 10, Apto. 10-4, S.C.', '439323 MERIDA O77-21', '', 1, 1),
(55, '5685657', 'JESUS ALEJANDRO', 'MORA PORRAS', 'Carrera 14 No. 10-54, Barrio Obrero, S/C.', '436078', '', 1, 1),
(56, '5686848', 'MARIO ALBERTO', 'TOVAR SIERRA', '.', '', '', 1, 1),
(57, '5687222', 'MAGALLY CECILIA', 'MONTIEL FINOL', 'Residencias Quinimari, Bloque 68, Apto. 5, S/C.', '550933', '', 1, 1),
(58, '5687497', 'HELMAN ALAN', 'MEDINA VARELA', 'Barrio Obrero, Calle 9, No. 15-76, San Cristobal', '0276 3439245 / 0414 ', '', 1, 1),
(59, '5688734', 'LUIS ENRIQUE', 'LANZA ESCALANTE', 'Urbanizacion bella vista vereda 9 bis casa 9 - 237', '0276  8084547', '0424  7388560', 1, 1),
(60, '6274745', 'JOSE ALBERTO', 'GUERRERO ORTEGA', '.', '', '', 1, 1),
(61, '6345106', 'WOLGFANG EDIXSON', 'MEJIA', 'Calle 10, entre Carrera 6 y Septima Avenida, No.6-44, S/C', '', '', 1, 1),
(62, '6365877', 'ANTOLIANO', 'RAMIREZ', 'Av. Lucio Oquendo, Edf. Frailejon, Apto. 5-A, S.C.', '472195 / 477828', '', 1, 1),
(63, '6400281', 'EUSTACIO', 'VASQUEZ MEJIA', '', '', '', 1, 1),
(64, '6507315', 'FELIX ENRIQUE', 'LEZAMA HERNANDEZ', 'Calle 1 No. 2-39, Palmira.', '940868', '', 1, 1),
(65, '6561304', 'ZOLTAN LASZLO', 'SZABO HERK', 'Coln. de la California, Av. Palmarito, Conj. Palmarito No. 16, CCS', '02 2577338 / 014 205', '', 1, 1),
(66, '6904525', 'JAVIER ANTONIO', 'HERNANDEZ HERNANDEZ', 'Carrera 2, No. 0-51, La Popita, Pueblo Nuevo, S.C.', '439587', '', 1, 1),
(67, '7234996', 'HENRY ARTURO', 'GALLARDO ORELLANA', '', '', '', 1, 1),
(68, '7656256', 'RAFAEL EDUARDO', 'USECHE MORA', 'Av. Madre Juana, Qta. Maria Eugenia, No.E-89', '439583 / 431804 / 43', '', 1, 1),
(69, '7896807', 'ATILIO SEGUNDO', 'DEL MAR NAVA', 'Urb. Los Teques, Edf. 21, Apto. 01-03, Piso 1, Santa Teresa, S/C.', '014 7046464', '', 1, 1),
(70, '7922061', 'JAVIER FRANCISCO', 'MERCHAN BURIEL', '8va. Av., Edf. Katherine, Pso.5, Apto. 5-1, La Concordia.', '476763', '', 1, 1),
(71, '7926676', 'JOSE GREGORIO', 'PADRON MONCADA', '', '', '', 1, 1),
(72, '8094700', 'ANECTO SIMON', 'MORA ROSALES', 'Urb. Cesar Morales, Sect. III, calle 20, No.12, El Palmar de la Cope', '', '', 1, 1),
(73, '8099722', 'OMAIRA', 'FIGUEROA DE M.', 'Calle Los Carreros Qta Sandra 2.Piso, Av. Ppal.Las Pilas Pueblo N.', '59455 / 562-363', '', 1, 1),
(74, '8109934', 'LUIS EDUARDO', 'TOCUYO ARELLANO', 'Vereda San Antonio, s/n, Colinas de San Rafael, via El Corozo', '', '', 1, 1),
(75, '8182618', 'PEDRO', 'RODRIGUEZ TARAZONA', '.', '', '', 1, 1),
(76, '8186591', 'LEOPOLDO', 'RODRIGUEZ CORONADO', '.', '', '', 1, 1),
(77, '8419141', 'RAMON CELESTINO', 'ARENAS VARGAS', 'Urb. San Sebastian , Bloque B4, Apto. 23', '', '', 1, 1),
(78, '8709704', 'RAFAEL EDICSON', 'GUILLEN VIVAS', 'Res. La Alameda, Torre 2, Piso 1, Apto. 1-1, S/C', '478389 (DEJAR MENSAJ', '', 1, 1),
(79, '8739079', 'SILVIA FELICIA', 'ALEJOS PACHECO', 'Vereda 04, casa 237', '0276-3480322', '', 1, 1),
(80, '8756895', 'MARCOS ISAIAS', 'CONOPOY', 'la castra, Av. principal, casa N° 2-20', '', '', 1, 1),
(81, '8769551', 'SAIRIO ALEXANDER', 'VILLEGAS ZAMBRANO', 'Urb. La Castra, Bque. 7, Pso.3, Apto.03-04, S.C.', '471365', '', 1, 1),
(82, '8987075', 'WILLIAM IVAN', 'JACOME BECERRA', '', '', '', 1, 1),
(83, '8989630', 'CIRO', 'ALVAREZ TORO', 'Calle 1, Casa NÂ° 29', '0276-3467718', '', 1, 1),
(84, '8990731', 'RUBEN DARIO', 'DAVILA', 'C. Los Colorados,Pje. J.Greg.Hernandez Vda.La Chinata, 0-49 La Conc.', '', '', 1, 1),
(85, '9025948', 'ZORAIDA JACKELINA', 'SANCHEZ MAHECHA', 'Colinas de San Rafael, Calle 2, s/n, via El Lano, S.C.', '872866 (MADRE)', '', 1, 1),
(86, '9134028', 'ALVARO FARID', 'HERRAN PEREZ', 'Urb. Unidad Vecinal, Calle 1, No. 24, S.C.', '463740 / 564870 / 47', '', 1, 1),
(87, '9149014', 'CARLOS ANTONIO', 'PEÑALOZA MARTINEZ', 'Av. Industrial, Conj. Res. Los Guásimos, Edif. 13, p.2, Ap. 01-02', '', '', 1, 1),
(88, '9206043', 'FREDDY JOSE', 'MONCADA VEGA', 'Palo Gordo, Calle Ppal. del Medio No. C-44 mas arriba Indus. Atenas', '', '', 1, 1),
(89, '9206087', 'RICHARD FREDDY', 'ORTIZ MORA', 'Resd. El Paseo, Pso. 1, Apto. 5, Urb. Paseo de las Ferias, Merida.', '074 403130 / 403131', '', 1, 1),
(90, '9206896', 'JOSE GREGORIO', 'JAUREGUI RODRIGUEZ', 'Urb. Cesar Morales Carrero, Calle 02, Sector 02, Casa No. 01', '447528 / 84175', '', 1, 1),
(91, '9207648', 'RAFAEL ANTONIO', 'CARDENAS ROSALES', 'Carrera 7 con Calle 11, No. 7-14, Independencia, Capacho.', '83148', '', 1, 1),
(92, '9209088', 'MARITZA', 'DELGADO HERNANDEZ', 'Urb. Cesar Morales, Calle 2, No.3, Sector II, El Palmar de la CopÃ©', '', '', 1, 1),
(93, '9209422', 'RAFAEL ANGEL', 'RAMIREZ BECERRA', '', '', '', 1, 1),
(94, '9209556', 'ORLANDO', 'BRICEÑO GARCIA', 'Urb. Cesar Carrero, Calle 4, No.2, Sector II, El Palmar de la Copé', '476103', '', 1, 1),
(95, '9210041', 'LUIS ALBERTO', 'ALFONSO CARRILLO', '', '', '', 1, 1),
(96, '9213004', 'JORGE ANTONIO', 'CHAVARRIAGA MESA', 'Calle 8, No. 14-43, Barrio Obrero, S.C.', '438251 / 014-9770240', '', 1, 1),
(97, '9214108', 'BELISARIO', 'GUILLEN SANCHEZ', 'Barrio Walter Marquez, Calle 2, No. 1, San Josecito', '', '', 1, 1),
(98, '9214703', 'OMAIRA', 'QUINTERO', 'Barrio MonseÃ±or Ramirez, Clle Ppal., No. 0-116, S/C.', '417739', '', 1, 1),
(99, '9217633', 'MIGUEL ANGEL', 'DUARTE CALDERON', 'El Llanito, Via Capacho, Vereda El Llanito, casa NÂ°15', '', '', 1, 1),
(100, '9219568', 'LUIS ALBERTO', 'SALCEDO CALDERON', 'Carrera 6 entre Calles 13 y 14, No 13-80, S.C.', '414485 / 416614 / 43', '', 1, 1),
(101, '9220032', 'JOSE ANTONIO', 'CONTRERAS BELANDRIA', 'Calle 2, No. 3-32, Barrio Pozo Azul, 23 de Enero, S.C.', '471213', '', 1, 1),
(102, '9220603', 'JAVIER ANTONIO', 'RAMIREZ RAMIREZ', 'Carrera 10, No. 4-40, Patiecitos', '911013', '', 1, 1),
(103, '9220783', 'NELSO ANTONIO', 'ZAMBRANO ZAMBRANO', 'vega de aza, calle 3, N° 14', '', '', 1, 1),
(104, '9222050', 'BERNARDO JOSE', 'CONTRERAS SOLANO', 'Urb. Las Acacias, Carrera 2, No. 2-49, S/C.', '557209', '', 1, 1),
(105, '9222412', 'BEATRIZ ELENA', 'CAMACHO', 'Colinas de San Rafael, Calle 2, s/n, via El Llano, S.C.', '', '', 1, 1),
(106, '9228962', 'ALEXANDER DOMICIO', 'ALCEDO RAMIREZ', 'Unidad Vecinal, Bque.4, Torre B, Apto. 1-B, S/C', '464442 / 014-7000572', '', 1, 1),
(107, '9232653', 'LUIS SEGUNDO', 'CAMACHO BOHORQUEZ', 'Calle 5 con Carrera 3, No. 3-49, Urb. Merida, S.C.', '471833', '', 1, 1),
(108, '9233070', 'MARCO ANTONIO', 'MENDEZ JAIMES', 'Urb. Toquito, Sector \"G\", No 171, Palmira.', '016 6765207 / 551177', '', 1, 1),
(109, '9233083', 'CARLOS MANUEL', 'PABON ROA', 'Calle 11, No. 82, Sta. Ana del Tachira.', '67206', '', 1, 1),
(110, '9233122', 'JOHNY', 'CRUZ MARQUEZ', 'Calle 3 No. 12, Sector IV,', '0276-3480010', '', 1, 1),
(111, '9234118', 'JOSE ARGENIS', 'ANGOLA ORTIZ', 'Av. Ppal. de Pueblo Nuevo', '422071', '', 1, 1),
(112, '9234741', 'IVAN ALEXIS', 'AMAYA RONDON', 'SABANETA CLL 2 CASA 29', '0276-5170158', '0416-4756901', 1, 1),
(113, '9235245', 'INGRID COROMOTO', 'BARRETO LOZADA', 'Carrera 10, No. 4-40, Patiecitos', '911013', '', 1, 1),
(114, '9237688', 'GEOVANNY ENRIQUE', 'PEREZ ARAQUE', 'Calle Ppal. de Las Vegas de Tariba, No. 6-36, Tariba', '943790', '', 1, 1),
(115, '9239028', 'LUIS ENRIQUE', 'BAUTISTA CANCINO', 'Calle 5, entre Carreras 6 y 7, No. 6-55, Táriba.', '557293', '', 1, 1),
(116, '9239032', 'ESTEBAN', 'PRATO CARREÑO', 'Calle El Dispensario, No. D-80, Sector La Toica, Palo Gordo.', '571323 - 572998', '', 1, 1),
(117, '9241333', 'GUSTAVO FABIAN', 'VILLAMARIN SAYAGO', 'Pasaje Acueducto, entre Carreras 13 y 14, No. 13-02, S.C.', '414787', '', 1, 1),
(118, '9241413', 'LEONARDO ERNESTO', 'LANZA ESCALANTE', 'Urb. Los Teques IV, Bque. 10, Pso. 3, Apto. 01-03, S/C', '417675', '', 1, 1),
(119, '9242163', 'YAJAIRA C.', 'SILVA DE ESCALANTE', 'Calle 3, No. 48, Sect. Araguaney, Gallardin, Palo Gordo.', '571801', '', 1, 1),
(120, '9243765', 'FRANK', 'TORRES', '', '', '', 1, 1),
(121, '9245123', 'ROSA JANETH', 'CASTRO CHACON', 'Carrera 4, No. 5-36', '', '', 1, 1),
(122, '9245531', 'GUSTAVO ADOLFO J.', 'MATHEUS VALDIVIESO', 'Urb. Las Acacias, Carrera 2, No. 3-41, S.C.', '478160 / 016-6763471', '', 1, 1),
(123, '9245980', 'FREDDY ALEXANDER', 'PEÑA OSMA', 'Urb. Los Teques, Bloque 18, Apto. 00-01, S/C.', '412530 / 412181', '', 1, 1),
(124, '9246620', 'JOSE HILDEMARO', 'GUTIERREZ SANCHEZ', 'Av. Ppal. de Pueblo Nuevo, Urb. Los Algarrobos, No. 26, S.C.', '445018', '', 1, 1),
(125, '9246793', 'GERMAN GREGORIO', 'HIGUERA ROSAS', 'Calle 11 con Carrera 14 y 15 No. 14-64 Barrio Obrero', '555381', '', 1, 1),
(126, '9246896', 'ADA RAQUEL', 'CAICEDO DIAZ', 'Urb. Bajumbal, Calle Piscuri, Qta. Niño Jesus, No. P-2, S/C.', '555660 / 016 6767567', '', 1, 1),
(127, '9247220', 'LUIS EDUARDO', 'DURAN GARCIA', 'Calle 11 No. 21 Barrio Obrero', '551255', '', 1, 1),
(128, '9247888', 'BELKYS JOSEFINA', 'GUERRA RAMIREZ', 'Carrera 9, Qta. Conchita, No. 0-130, Barrio Ambrosio Plaza. P.Nuevo', '532358 / 016 6764483', '', 1, 1),
(129, '9248125', 'DAVID RAMON', 'GONZALEZ CARDENAS', 'Calle \"B\", No. 88, Sta. Marta, Los Naranjos, S.C.', '563853', '', 1, 1),
(130, '9249765', 'JESUS ALBERTO', 'CARDENAS VERA', 'Calle 2, No. 1-74, Barrio Sucre,  S/C', '016 6761205', '', 1, 1),
(131, '9349642', 'MARCO ANTONIO', 'MEDINA SALAS', 'Res. Balmoral, Torre 0, Piso 1 Apt. No. 3, Av. Las Pilas, Pueblo Nue', '', '', 1, 1),
(132, '9392939', 'YEINI OLIVA', 'VARGAS LABRADOR', 'Calle 16, No.20-26 entre carrera 20 y 21, Barrio Obrero', '550239', '', 1, 1),
(133, '9463495', 'MARIA TERESA', 'LAGE RODRIGUEZ', 'Av. Ppal. de Pueblo Nuevo, Resd. El Bosque, Apto. 6-2A, S.C.', '445881', '', 1, 1),
(134, '9467407', 'PEDRO REINALDO', 'ZAMBRANO RODRIGUEZ', 'Calle 6 No. 6, Urb. Misia Juana, El Poblado, Rubio.', '624668', '', 1, 1),
(135, '9469043', 'EVENCIO ALEJANDRO', 'TRUJILLO OCARIZ', 'Calle 07, No. 18-45, Barrio Lourdes.', '557542', '', 1, 1),
(136, '9469237', 'JOSE A.', 'GELVEZ BELTRAN', 'La Popa, via Granjas Infantiles, Calle Ppal. No. 20-65, S.C.', '432402', '', 1, 1),
(137, '9487582', 'HENRY REINALDO', 'OROZCO BUENO', 'VIA PRINCIPAL DE CAPACHOPARTE ALTA , CASA B-75, EL JUNCO, TARIBA', '0276-3946851', '0414-7057364/0412428', 1, 1),
(138, '9513290', 'ELSIE ELENA', 'SALAZAR DE MERCHAN', '8va. Av. La Concordia, Ed. Catherine, Piso 5, Apto. 5-1', '476763', '', 1, 1),
(139, '9644559', 'JUAN VICENTE', 'SALAZAR HUERFANO', 'Urb. Altos de Paramillo, Manzzana 7, Qta. No. 3', '014 7042116', '', 1, 1),
(140, '9973442', 'JESUS DANIEL', 'CORDOVA FUENTES', 'Calle 3 No. 13-75, Qta. La Foly, La Concordia, S/C.', '472410 / 014-9765625', '', 1, 1),
(141, '10013565', 'HIPOLITO GREGORIO', 'CHOURIO CHUELLO', 'Barrio Obrero, Carrera 20, entre calles 14 y 15, No. 14-39. S/C.', '559291', '', 1, 1),
(142, '10075883', 'ESTEVAN ERASMO', 'FAJARDO OROZCO', 'Pinares del Torbes, calle principal, NÂ° 2-58', '', '04168700530', 1, 1),
(143, '10119745', 'EFRAIN', 'RAMIREZ USECHE', 'El Tambo, entrada El Palmar Ramireño, No. 2, via Sta. Ana del Tach.', '', '', 1, 1),
(144, '10145114', 'JOSE GREGORIO', 'ZAMBRANO CANCHICA', 'Agua Blanca, Capacho / Libertad, Edo. Tachira', '463758', '', 1, 1),
(145, '10145150', 'CARMEN ALICIA', 'SANTAFE ACEVEDO', 'Avda. Carabobo con Avda. España, Canal subiendo, Quinta Leonor.', '552594', '', 1, 1),
(146, '10145684', 'GERSON JOSE', 'MOLINA JAIMES', 'Calle 3, No. 35, Qta. Yolmar, Urb. El Sinaral, S.C.', '556364', '', 1, 1),
(147, '10145925', 'DANIEL', 'LIZCANO CONTRERAS', 'Av. Principal de Pueblo Nuevo, Barrio Buenos Aires, No.0-19', '564861 / 014 7022281', '', 1, 1),
(148, '10149071', 'FRANK ABELINO', 'BORRERO ALVIAREZ', 'El Valle Sector Urrego, Calle Mons. Parada', '', '', 1, 1),
(149, '10152067', 'ALBA MARINA', 'HERNANDEZ', 'Barrio Rafael Moreno, Parte Alta, No.0-51, Sector El Rio', '', '', 1, 1),
(150, '10153305', 'CESAR AUGUSTO', 'GUTIERREZ SANCHEZ', 'Av. Ppal. de Publo Nuevo, Urb. Los Algarrobos, No. 26, S.C.', '445018', '', 1, 1),
(151, '10153343', 'DARWING HUMBERTO', 'GONZALEZ ARVELO', 'Conj. Resd. La Pradera, Calle 0, Qta. Tata, Tucape.', '422409 / 014-7041870', '', 1, 1),
(152, '10153650', 'FREDDY ALEXANDER', 'BECERRA SUAREZ', 'Vega de Aza calle 2 con carrera a 5 NÂº 21250', '3478735 / 3430922 (M', '04166010915', 1, 1),
(153, '10154574', 'JOSE ALEXIS', 'VIVAS SANCHEZ', 'Terrazas del Palmar, El Palmar de la Cope.', '', '', 1, 1),
(154, '10154598', 'JANETH TERESITA', 'PAREDES DURAN', 'Carrera 1 No. 1-36, Barrio Alianza', '462732 / 446807', '', 1, 1),
(155, '10156632', 'PEDMAR ALEXANDER', 'LOBO GALAVIS', 'Urb. Los Naranjos, Calle C, No. 10, S/C.', '563163', '', 1, 1),
(156, '10157078', 'LUIS ALFONSO', 'LEAL BLANCO', '', '', '', 1, 1),
(157, '10158226', 'EDIT MILAGROS', 'DUQUE CONTRERAS', 'C.C. y Resd. Plaza Suite San Cristobal, Pso. 2, Apto. 2-6, S/C', '0416 8785538 / 0276 ', '', 1, 1),
(158, '10160119', 'REGULO JOSE', 'LOBO VILLASMIL', 'Urb. Nueva Guayana, Sector A, Calle 4 No. 18, Qta. El Pedregal, S/C', '439584 / 014-9768824', '', 1, 1),
(159, '10161669', 'RAFAEL JOSE', 'MEDINA LOZADA', 'Av. Ppal. de La Popita, No. 6-87, Pueblo Nuevo, S.C.', '436843', '', 1, 1),
(160, '10162120', 'LUIS EGARDO', 'SANCHEZ VIVAS', 'Barrio El Lobo, Calle 3 No. 0-160, S/C.', '0276-3564535', '04268762427', 1, 1),
(161, '10164141', 'ISBELT ROSARIO', 'ANGULO COLOMBO', 'Calle 3 No. 12, Sector IV, Urb. C.M.C., El Palmar de la CopÃ©', '0412-1696815/0414 70', '', 1, 1),
(162, '10164142', 'MIGDALIA LIZETH', 'ANGULO COLOMBO', 'B. Monseñor Briceño, carrera 10 No. 1-70, Táriba.', '942529', '', 1, 1),
(163, '10166282', 'HENRY SMITH', 'CONTRERAS DUARTE', 'Carrera 12 No. 1-22, La Guacara, S/C', '443475', '', 1, 1),
(164, '10166388', 'MAGALLY DEL C.', 'RODRIGUEZ SALINAS', 'Calle Trinidad, No. T-38, Barrancas, Parte Alta.', '', '', 1, 1),
(165, '10167372', 'ALFREDO', 'SALAZAR ROJAS', '.', '', '', 1, 1),
(166, '10172683', 'CHRISTIAN ALEXIS', 'VILLAMIZAR', 'Pirineos II, Bloque 23, Apto. 02-03, S.C.', '556007', '', 1, 1),
(167, '10173657', 'LITZY SOLANGGE', 'RODRIGUEZ RODRIGUEZ', 'Carrera 4, No. 38, Santa Ana, Edo. Tachira', '557180 / 462361', '', 1, 1),
(168, '10173894', 'EDGAR', 'SILVA ROA', 'Barrio Santa Cecilia, Calle 1 Bis A No. 3-37, S/C.', '562809', '', 1, 1),
(169, '10174054', 'ALEXANDER OMAR', 'GAMEZ PARRA', 'Urb. Rafael Urdaneta, Parte Alta, Vereda 4, No.64-99, S/C', '016 7775781 / 478510', '', 1, 1),
(170, '10174250', 'LILIANA ASTRID', 'PEREZ FLORES', 'Carrera 3 No. 0-13, Barrio Ambrosio Plaza Pueblo Nuevo', '562516 / 014-7004232', '', 1, 1),
(171, '10175246', 'SANDERSON L.', 'CHACON PARRA', 'Carrera 7 con Calle 5, No. 6-130, Urb. Propatria, La Concordia, S.C.', '477236', '', 1, 1),
(172, '10176429', 'OLIVER ONASSIS', 'MOLINA SANCHEZ', 'Av. Lucio Oquendo Torre Europa Piso 2 Apto. A3', '465226', '', 1, 1),
(173, '10176760', 'GIAN FRANCO', 'FARACO GALLANTI', 'via Cafetal con Pasaje El Tejar, casa NÂ° C-31', '0276-3551443', '', 1, 1),
(174, '10177740', 'YORLEY JANETH', 'CANCHICA MONCADA', 'Urb. Pirineos I, Lote C, Vereda 13 No. 7, S/C.', '568749', '', 1, 1),
(175, '10178623', 'JUAN ALBERTO', 'COLMENARES GERMAN', 'Barrio Bolivar, Calle 6 No. 28-58', '565814', '', 1, 1),
(176, '10179069', 'LUIS ENRIQUE', 'MORALES RANGEL', 'Barrio 23 de Enero, P/A, Pasaje Colombia, No. 7-32, S/C', '474272 / 415221 / 41', '', 1, 1),
(177, '10190175', 'LUZ MARINA', 'RINCON JAIMES', 'Barrio San Sebastian, Calle 1, No. 1-09, La Concordia, S.C.', '476835 / 018 7600004', '', 1, 1),
(178, '10481311', 'LISETT LIDEMAR', 'RINCON ECHEZURIA', 'Paramo de La Sabana, Rancho Miss Nelyda, Rancheria, Via Capacho.', '', '', 1, 1),
(179, '10539761', 'LIANA ESTHER', 'OCHOA DELGADO', 'Puente Real, Pasaje Yagual, No. 10-43, S/C.', '436255', '', 1, 1),
(180, '10800866', 'SADAR EMIRO', 'CASTILLO PEÑA', 'Calle 12, No. G-22, entre Pasajes Cumana y Guasdualito, Puente Real.', '415146', '', 1, 1),
(181, '10801186', 'RICHARD ALBERTO', 'SANCHEZ VARGAS', 'Palmar de la Cope, Sector 2, NÂ° 01, Cesar Morales Carrero', '0276-5168707 / 0276-', '', 1, 1),
(182, '10849247', 'DANIEL ENRIQUE', 'MAJANO JIMENEZ', 'Prolog. de la Unidad Vecinal, Vereda 8, No. 39, S/C', '475502', '', 1, 1),
(183, '11106171', 'YULSMI LISBETH', 'RODRIGUEZ SANCHEZ', 'Lote B, Vereda 24 No. 12 Pirineos I S/C', '567287 / 014-7065398', '', 1, 1),
(184, '11107204', 'FRANCIA HAYDEE', 'PEÑA DUARTE', 'Urb. El Centenario, Calle 6 No. 10, Santa Ana.', '', '', 1, 1),
(185, '11110385', 'WILMER ROLANDO', 'RAMIREZ VALENCIA', 'Calle principal de palo gordo #1-73C Gallardin al lado de la escuela', '', '', 1, 1),
(186, '11111132', 'OSWALDO ENRIQUE', 'CONTRERAS', 'VEREDA 11, LAS ROSAS, LOMAS BLANCAS', '6113391', '', 1, 1),
(187, '11112992', 'DIANA ISABEL', 'RUEDA QUICENO', 'Tonono, Via Rubio, Km. 2, Vereda 1, Casa S/N, S/C.', '014 7004322', '', 1, 1),
(188, '11113204', 'YELSA LILIBETH', 'RODRIGUEZ SANCHEZ', 'Pirineos I, Lote B, Vereda 24, No. 12, S/C', '559327', '', 1, 1),
(189, '11113324', 'EDUILMER', 'CORONADO MESA', 'Carrera 4, 2da. vereda, No. 2-00, La Concordia, S.C.', '472883', '', 1, 1),
(190, '11288236', 'ANA EMILVA', 'ROMAY DE GOMEZ', '', '', '', 1, 1),
(191, '11490227', 'WINDER RAFAEL', 'CANACHE ROSALES', 'Barrio Las Flores, S/C.', '', '', 1, 1),
(192, '11491814', 'HUMBERTO AMATO', 'ABATE CAICEDO', 'altos de paramillo, sector cueva del oso, calle el pensamiento, casa N°9', '0276-3531728', '', 1, 1),
(193, '11491921', 'SANDRA PATRICIA', 'CAMACHO GUTIERREZ', 'CALLE 15 NRO. 11-52 CON CARRERA 11 Y 12 BARRIOS SAN CARLOS', '', '', 1, 1),
(194, '11492835', 'JOSE ALEXANDER', 'BELTRAN DEPABLOS', 'Carrera 4 No. 1-23, 23 de Enero', '464687', '', 1, 1),
(195, '11493365', 'ELBA JETZABEL', 'AMAYA ROJAS', 'Carrera 3 A, Nº 3-40, Pueblo Nuevo, Prolongacion Barrio El Paraiso, Quinta Jetzabel', '0276-3438221', '', 1, 1),
(196, '11494193', 'NIRIAM JANETH', 'RAMIREZ JAIMES', 'Av. Ind. de Paramillo, Sector Sta. Cecilia, Vda. 1, No. V-5, S/C.', '565505', '', 1, 1),
(197, '11495682', 'CARMEN ISBELIA', 'VERA LUNA', 'El Mirador, Via Rubio, Calle Cipriano Castro, lado finca Los Pinios.', '', '', 1, 1),
(198, '11495833', 'NERIO URIAK', 'CARRERO CASANOVA', 'Final Calle Los Duques, s/n, Barrancas Parte Alta.', '443298', '', 1, 1),
(199, '11497387', 'EDWING EPHRAIM', 'GUERRERO CARDENAS', 'Calle 3, No. 28-32, Qta. Madelein, Urb. Coromoto, La Concordia, S.C.', '479491', '', 1, 1),
(200, '11497442', 'MARCIAL ANTONIO', 'MANTILLA VILLAMIZAR', 'Km. 7 via Rubio, Caserio El Porvenir Estado Tachira', '0276-5168338', '', 1, 1),
(201, '11497472', 'ORANGEL RAHIMIR', 'URBINA LABRADOR', 'Calle 4, No. 5-62, El Diamante (parte baja), Tariba', '940663', '', 1, 1),
(202, '11498134', 'JESUS MAURICIO', 'MONCADA PALOMINO', 'Urb. La Vega, No.    , Vega de Aza.', '', '', 1, 1),
(203, '11500642', 'JOSE LUIS', 'CHACON CHACON', 'AV. PAEZ CALLE 10 NRO. 2-13', 'N/A', '', 1, 1),
(204, '11500869', 'MITZI MARIEL', 'DELGADO MEDINA', 'Barrancas, P/B, Calle 9 No. 3-6, S/C', '3431649', '', 1, 1),
(205, '11500943', 'ANA JANETH', 'VALBUENA ROMERO', '.', '', '', 1, 1),
(206, '11501385', 'ANA YANERIS', 'COLMENARES', 'Palo Gordo, calle Principal de Gallardin, casa N° 1-7c', '0276-3572090', '', 1, 1),
(207, '11501461', 'YIMI KRISTIAN', 'MADERO CRISTANCHO', 'El Abejal, Entrada Ppal. Campo Deportivo, Palmira', '014-7003475', '', 1, 1),
(208, '11502359', 'HAROL ELIAS', 'MEZA', 'Pirineos II, Bloque 14, Apto. 01-02, S/C.', '554240', '', 1, 1),
(209, '11502492', 'ALFREDO ENRIQUE', 'ZAMBRANO BERBESI', 'Av. Los Agustinos, Calle El Alto, No. 08-A, Apto. 1, Paramillo, S/C.', '018 7656271 / 394061', '', 1, 1),
(210, '11502931', 'GREGORY ANTONIO', 'RAMPALY RANGEL', 'Urb. Quinimari, Bloque 40, Apto. 4, Pirineos, S/C', '0416 4756632 / 35333', '', 1, 1),
(211, '11503232', 'ERIKA MARLENE', 'WILCHEZ DE SALCEDO', '', '439885 / 416614', '', 1, 1),
(212, '11503376', 'NESTOR ALFREDO', 'PEREIRA LOPEZ', 'Calle Sucre, Resd, Los Monges, Qta. Maria Eugenia, No. 2, Cordero.', '85554', '', 1, 1),
(213, '11505503', 'ANGEL ELIECER', 'PADRON AYALA', 'Calle 7 entre carreras 8 y 9, No. 2-18, Sta. Eduviges, Tariba.', '462120', '', 1, 1),
(214, '11506097', 'LUIS OSCAR', 'RAMIREZ VILLAMEDIANA', 'Avenida Principal, No. 83,', '555311', '', 1, 1),
(215, '11506210', 'JOSE VICENTE', 'CRUZ MENDEZ', 'Calle 11 No. 19-31, Barrio Obrero, S/C', '557682 / 559513', '', 1, 1),
(216, '11506334', 'PABLO EMILIO', 'MONCADA ANAYA', 'Barrancas parte baja, Pasaje Sucre, No. 3-109, S/C', '', '', 1, 1),
(217, '11506335', 'PAUL GERARDO', 'MONCADA ANAYA', 'El Hiranzo, Parte Alta, Calle Las Flores, No. 2-32, Tariba.', '433962', '', 1, 1),
(218, '11506608', 'ALEXANDER ENRIQUE', 'RAMIREZ', 'Calle Altamira, No. A-40, Barrancas, parte alta, Tariba.', '416421', '', 1, 1),
(219, '11507629', 'PABLO ERNESTO', 'RAMIREZ LUNA', 'Toiquito, via Palmira, Vereda Los Proceres, No. B-18, Palmira.', '944245', '', 1, 1),
(220, '11507814', 'DORLEY JOSMAR', 'RUIZ LOPEZ', 'Carrera 14, No. 6-39, entre calles 6  y  7', '0276-3431252', '0416-6742504', 1, 1),
(221, '11508381', 'NELSON AUGUSTO', 'SERRANO MONSALVE', 'Patiecitos, calle 4 carrera 1 No.3-94, mas abajo del Vivero patiecit', '0414 7085415 / 0414 ', '', 1, 1),
(222, '11509162', 'PABLO CESAR', 'VIVAS', 'Carrera 6, No. 13-24, Barrio Mons. BriceÃ±o, Tariba.', '942514', '', 1, 1),
(223, '11525553', 'ARNALDO RAMON', 'DYONGH SOSA', '.', '014 7077568 / 560687', '', 1, 1),
(224, '11838884', 'RAMON UPONINEZ', 'ANCENO GARCIA', '.', '', '', 1, 1),
(225, '11840770', 'RICHES HUMBERTO', 'MOLINA ROJAS', '', '', '', 1, 1),
(226, '11953981', 'NELSON GABRIEL', 'TELLEZ CANRO', 'Barrio Colón, Resd. Monterrey, Edf.10, Pso.2, Apto.8, S/C.', '444397 (H)', '', 1, 1),
(227, '12219004', 'NORIS DEL CARMEN', 'CONTRERAS PAREDES', 'Caserio Altos de Tonono, Km. 3, via Rubio.', '', '', 1, 1),
(228, '12227565', 'HUMBERTO ERASMO', 'MARQUEZ PEREZ', 'Carrera 7, No. 3-39, La Concordia, S/C.', '3472983 / 3565469', '', 1, 1),
(229, '12228690', 'ZULLY YALITZA', 'ANGOLA COLINA', 'SANTA ANA CARRERA 4 ENTRE CALLES 14 Y 15, QUINTA ESTHER #14-77', '3551897/7667875', '', 1, 1),
(230, '12229160', 'RONALDO ALBERTO', 'GUERRERO MORENO', '', '0276-3469042, 0276-3', '', 1, 1),
(231, '12229290', 'RITA ELISA', 'ZAMBRANO ROMERO', '.', '', '', 1, 1),
(232, '12229721', 'OSCAR', 'HERNANDEZ', '.', '0276-4151336', '0424-7341378', 1, 1),
(233, '12229835', 'JACKSON ISANDRY', 'PINTO PINILLA', 'Calle 4 bis, No. 9-15, La Concordia, S.C.', '462312', '', 1, 1),
(234, '12229844', 'JHONNY ENRIQUE', 'RUIZ LOZANO', 'Vereda 21, No. 7, Parte baja, Urb. Sucre, S.C.', '555798', '', 1, 1),
(235, '12230365', 'WILMER ALBERTO', 'VARGAS RONDON', 'Carrera 6, No. 5-49, Táriba.', '940873', '', 1, 1),
(236, '12230675', 'MAYELA VICTORIA', 'PEREIRA PEREZ', 'Barrio Libertador, Calle 3, No. 4, parte alta, S.C.', '556591 / 014-7003697', '', 1, 1),
(237, '12230977', 'GIUSTO ANTONIO', 'GONZALEZ VARELA', 'Calle El Medio, Pasaje Porvenir, No. C-48, Palo Gordo, S.C.', '572141', '', 1, 1),
(238, '12231013', 'VICTOR HUGO', 'ZAMBRANO MORALES', 'Sector La Cueva, via Hosp. Militar, Calle Senda La Moralera, s/n.', '562394 / 564029', '', 1, 1),
(239, '12232871', 'JUAN CARLOS', 'REYES', 'Urb. Las Lomas, Av. Falcon, Qta. Mi Querencia, S.C.', '411168', '', 1, 1),
(240, '12233002', 'RICHARD IVAN', 'MENDEZ ZAMBRANO', '', '', '', 1, 1),
(241, '12233274', 'YADIRA EMILEYDA', 'LEAL DELGADO', 'Calle Ppal. El Medio, Palo Gordo, Barrio El Cafetal, No. C-9, S/C', '572998 / 572020 / 01', '', 1, 1),
(242, '12233292', 'ALEJANDRO JOSE', 'RICO CASTRO', 'Urb. Santa Teresa, Bloque \"B\", Apto. No. 5, S/C.', '445873 / 446767 / 01', '', 1, 1),
(243, '12233399', 'FELIX JOSE', 'RICO CASTRO', 'Bloque B, Apartamento 5', '0276-3445873', '', 1, 1),
(244, '12234692', 'ANYELO ELI', 'URBINA GOMEZ', 'Calle 11 Bis, No. 8-61, Barrio Mons. BriceÃ±o', '0276-3941548', '', 1, 1),
(245, '12235045', 'LUIS ANDRES', 'MOLINA BECERRA', 'Calle 01, No. 36, Urb. Sucre, S.C.', '556852', '', 1, 1),
(246, '12264100', 'JORGE EDINSON', 'ROJAS GOMEZ', 'Calle 2 bis, carrera 6, NÂ° 1, Resid. Nelisa, Pueblo Nuevo', '3532560', '0414-7185825', 1, 1),
(247, '12447221', 'FERNANDO JAVIER', 'CONTRERAS', 'Vereda 11 Colinas de toiquito palmira', '016 8768754/276 5160', '', 1, 1),
(248, '12516597', 'LUIS ALEXIS', 'SALAZAR CONTRERAS', 'Urb. La Castra, Bque. 11, Apto. 01-05, S.C.', '460761', '', 1, 1),
(249, '12517569', 'DICKSON ANDRES', 'GOMEZ', 'Calle 1 casa nº 2-78 ', '7622865', '04265731817', 1, 1),
(250, '12554967', 'MIGUEL ANGEL ', 'CALLES GERMAN ', '.', '', '', 1, 1),
(251, '12630339', 'JESUS ORLANDO', 'FLORES CONTRERAS', 'Bloque 6, Pso. 8, Urb. La Castra, S.C.', '464062', '', 1, 1),
(252, '12630719', 'DHENIS ALDEMAR', 'BUSTAMANTE', 'Carrera 3, No. 2-111, La Popita, S.C.', '441593 (VECINA) / 56', '', 1, 1),
(253, '12631270', 'WILLIAMS SALOMON', 'VERA LUNA', 'El Mirador, Via Rubio, Calle Cipriano Castro, No. 209, S/C', '', '', 1, 1),
(254, '12632049', 'NERIO RAMON', 'CHACON CONTRERAS', 'Cordero, calle Bella Vista, Vereda 6, N° 1-113', '0276-3472939, 0276-5', '', 1, 1),
(255, '12632624', 'CARMEN SELENY', 'MARQUEZ DE TOCUYO', 'Calle 2 No. 2-65, Barrio Libertador, S/C.', '555137', '', 1, 1),
(256, '12632701', 'NEYLA ELIZABETH', 'CARDENAS CARDENAS', 'Carrera 1 No. 1-52, Barrio Libertador, S/C.', '567697 / 554925', '', 1, 1),
(257, '12633148', 'LIZBETH MILENA', 'ARIAS SANCHEZ', 'Urb. San Sebastian, Bloque B-1, Apto. 42, S/C.', '460130', '', 1, 1),
(258, '12633588', 'DEISY CECILIA', 'LABRADOR ALVAREZ', 'Pirineos I Lote F Vereda 17 # 41, S/C.', '014 9775444', '', 1, 1),
(259, '12634089', 'NELSON ENRIQUE', 'QUINTANA MARTINEZ', 'Colinas del Torbes, Transversal 1, NÂ° 2-58', '', '', 1, 1),
(260, '12634659', 'INGRID MAILO', 'GAMBOA DE MEJIA', 'Palmar de la Cope, Urb. Altos del Palmar, No. 124.', '', '', 1, 1),
(261, '12813710', 'SHIRLEY TERESA', 'CONTRERAS GUERRERO', 'Calle 1, Vereda 4, No. 4-60, Madre Juana, S/C', '436297', '', 1, 1),
(262, '12813292', 'LAIDY ZULEIMA', 'RENDON GUERRERO', 'S/C', '', '', 1, 1),
(263, '12815044', 'DARCY ESMERALDA', 'RANGEL CALDERON', 'Urb. San Sebastian, Bloque E Apto. 24', '466543', '', 1, 1),
(264, '12815279', 'FANNY COROMOTO', 'RAMIREZ JAIMES', 'Av. Ppal. Pueblo Nuevo, Barrio Buenos Aires, No. 54-10, S/C', '014 7063450', '', 1, 1),
(265, '12815327', 'CARMEN XIOMARA', 'RAMIREZ JAIMES', 'Barrio Buenos Aires, Calle Ppal, No. 54-10, Pueblo Nuevo', '564459', '', 1, 1),
(266, '12815879', 'HENDERSSON JAVIER', 'GIMENEZ MARQUEZ', 'Carrera 12 con Calle 13, No. 13-26, Barrio San CArlos', '', '', 1, 1),
(267, '12816410', 'WILMER ENRIQUE', 'PEREZ LAGOS', 'Pueblo NUevo, Barrio Ambrosio Plaza, Calle 1, No. 2-130, S.C.', '563667', '', 1, 1),
(268, '12970108', 'NILDA NOHEMI', 'CONTRERAS CASTELLANOS', 'Palmar de la Cope, sector 4, calle 6, casa NÂ°15', '', '', 1, 1),
(269, '12971590', 'ROSA DELIA', 'BATECA DE ROMERO', 'Barrio Union, No. PN 1673, Pueblo Nuevo,', '', '', 1, 1),
(270, '12973428', 'LORENA KARINA', 'TORRES GAMBOA', 'Calle 3 No. 4-76, frente al Cementerio, Santa Ana.', '', '', 1, 1),
(271, '12973518', 'ELDA YASMIN', 'RIVAS JAIMES', 'Calle 15 con Carrera 8, No. 8-18, S/C.', '414886', '', 1, 1),
(272, '12973894', 'YARITZA ISBEL', 'PEÑA DUARTE', 'Urb. El Centenario, Calle 6 No. 10, Santa Ana.', '', '', 1, 1),
(273, '12974300', 'FREDDY JESUS', 'CONTRERAS FERREIRA', 'santa teresa, vereda 3, numero 2-5', '0276-3412040', '', 1, 1),
(274, '13014794', 'JOSE ALFREDO', 'ESCALANTE GONZALEZ', 'Barrio Bolivar, Calle Ppal. No. 1-6, S/C.', '562418', '', 1, 1),
(275, '13098848', 'JORGE LEONARDO', 'RIOS PRADA', '.', '', '', 1, 1),
(276, '13145345', 'SANDRA JUDITH', 'PEREZ CARRERO', 'Pasaje La Catedral, Vereda 6, No. G-03, S/C', '415362', '', 1, 1),
(277, '13146044', 'PABLO JESUS', 'RUBIO CARDENAS', 'Av. Ppal Pueblo Nuevo, Carrera 1 No. PN-124, S/C (al Lado Res. Este)', '563020', '', 1, 1),
(278, '13147538', 'EDUAR EDIXON', 'GARCIA BARRERA', '.', '', '', 1, 1),
(279, '13147828', 'JORGE GABRIEL', 'COLMENARES', 'Calle 12, Carr. 19 y 20, No. 19-78, Barrio Obrero, S/C.', '557194 / 014-7004151', '', 1, 1),
(280, '13349202', 'TRINO KEWYN', 'ROMERO SALAS', 'Carrera 18, No. 7-23, Barrio Lourdes, S.C.', '560078 / 563545', '', 1, 1),
(281, '13350749', 'JUAN CARLOS', 'GARNICA COLMENARES', 'CALLE 8 CASA NÂº. 8-30 TUCAPE SAN CRISTOBAL', '0276-8085986', '04167725671', 1, 1),
(282, '13351685', 'JOSE ORLANDO', 'USECHE CASTRO', 'Sector Llano de La Cruz, parte alta, No. 10-48, Cordero.', '014 7015930', '', 1, 1),
(283, '13467728', 'XAVIER RICARDO', 'VERA HERNANDEZ', 'Conj. Resd. Monterrey, Edf. 1, Pso. 2, Apto. 10, La Guayana, S.C.', '448372', '', 1, 1),
(284, '13467985', 'SIGLINDA ELENA', 'JIMENEZ NAVAS', '', '', '', 1, 1),
(285, '13468065', 'JORGE DENNIS', 'ANGEL TIVAMOSO', 'CALLE 12 PASAJE GUASDUALITO N. G-4', '3479854', '04147488040', 1, 1),
(286, '13468351', 'JUAN CARLOS', 'MORERA MARTINEZ', 'Caneyes, Vereda Los Almendros, S/N, Palmira, Mun. Guasimos', '0416 7091171 / 0276 ', '', 1, 1),
(287, '13529472', 'ALIRIO JOSE', 'GOMEZ DEL VILLAR', 'La Pradera, final Calle 11, No. 11-11 Cordero.', '', '', 1, 1),
(288, '13549930', 'CRISMAR NATANIELLA', 'CLAVIJO', '', '', '', 1, 1),
(289, '13550045', 'DORA BETULIA', 'BORJAS URUEÑA', 'RECIDENCIAS DON LUIS EDF. RIO URIBANTE, PISO 06 APTO.6-2, ', '0276-6724993', '0414-9767975', 1, 1),
(290, '13708827', 'JAHSON DAVID', 'RAMIREZ ZAMBRANO', 'Calle 3 No. 2 - 59 Barrio Libertador', '02763561935', '04247619519', 1, 1),
(291, '13708831', 'DOMERICA', 'BECERRA ROSALES', 'Bloque 18, Pso. 2, No. 02-08, Urb. La Castra, S.C.', '468180', '', 1, 1),
(292, '13709976', 'HENRY CRISTHIAN', 'MONSALVE ZAPATA', 'El Helechales, Calle El Arbol, No. H-2, Palo Gordo, S.C.', '572374 / 014 7017074', '', 1, 1),
(293, '13792405', 'ENYER YOVANI', 'PARRA PINEDA', 'Carrera 5 No. 7-27, La Concordia, S/C.', '', '', 1, 1),
(294, '13809364', 'MARIA GABRIELA', 'CONTRERAS PEREZ', 'Calle 2 casa No. 5', '0276-3567227', '', 1, 1),
(295, '13891850', 'CHRISTIAN ERMINSON', 'GONZALEZ DURAN', 'BLOQUE 05 PISO 2 APTO 05-03 SANTA TERESA', '', '', 1, 1),
(296, '13898486', 'JESUS ALEJANDRO', 'CARRERO JARQUIN', 'calle 6, carrera 15, NÂ° 15-21, la guacara', '0276-8083595', '', 1, 1),
(297, '13917888', 'DAYNE DEL ROSARIO', 'LOPEZ RODRIGUEZ ', '', '', '', 1, 1),
(298, '13939172', 'JAIRO ALONSO', 'OVALLOS CAÑAS', 'CARRERA 2, CASA 0-60', '', '', 1, 1),
(299, '13972209', 'ALVARO LUIS', 'GOMEZ BOTELLO', '.', '', '', 1, 1),
(300, '13972604', 'SIGILFREDO JOSE', 'RUIZ', '', '', '', 1, 1),
(301, '13972759', 'SONIA', 'LAVAO RAMIREZ', 'Barrio Alianza, Calle 4, Carrera 4 No. 4-40, S/C.', '014 7013618', '', 1, 1),
(302, '13973473', 'JESUS EDGARDO', 'MORENO URBINA', 'Barrio San Cristobal, Vereda 4, No. 4-16', '', '', 1, 1),
(303, '14042492', 'LUDDY YASMIN', 'VANEGAS MEDINA', 'Urb rincon de la Vega NÂº 21250 Vega de Aza', '276-3478735', '04166011021', 1, 1),
(304, '14042818', 'EDGAR MANUEL', 'BORRERO SILVA', 'Carrera 9 Panamericana, No. 9-98 (2do. piso) Patiecitos.', '', '', 1, 1),
(305, '14042852', 'PEDRO SEGUNDO', 'MORILLO CHACON', 'Barrio Genaro Mendez Casa s/n', '3479179', '', 1, 1),
(306, '14099522', 'JONNY ALEXANDER', 'MENDOZA GARCIA', 'Est. Santa Ana, via parque la Petrolia, casa N° 1-146', '0276-5164375', '', 1, 1),
(307, '14099996', 'IRWIN RONALDO', 'RODRIGUEZ MORENO', '', '', '', 1, 1),
(308, '14100905', 'DEYSY YOLIMAR', 'CHACÓN DÍAZ', 'Casa No. 15, Lomas Blancas', '0276-4143995', '', 1, 1),
(309, '14179304', 'MONICA ANDREA', 'MADERO CRISTANCHO', 'Entrada Ppal. Campo Deportivo Palmira, No. 1-27, S.C.', '', '', 1, 1),
(310, '14179583', 'LUIS SIGFREDO', 'YANEZ BAUTISTA', 'calle principal de la cueva del oso, casa NÂ° 61-7', '', '', 1, 1),
(311, '14180694', 'HUGO ALEJANDRO', 'MORALES GIL', 'Car. 1 # 0-21, Qta. Las Adelitas, Urb. Colinas Bello Monte, P. Nuevo', '', '', 1, 1),
(312, '14348593', 'KERLY CAROLIN', 'RICO BOADA', 'Las Margaritas, Sector F, Vereda 3 No. S-20, Tariba', '014 7085594 / 394329', '', 1, 1),
(313, '14349381', 'KARINA ZULEIMA', 'GALINDO', '.', '', '', 1, 1),
(314, '14418471', 'MARYORY', 'ESCALANTE GUERRERO', 'CALLE 4 BIS Nº 8-104 LA CONCORDIA', '', '', 1, 1),
(315, '14605146', 'ANGIE', 'HEVIA QUIROZ', 'CARRERA 13 ENTRE CALLES 12Y13 #12-86 ', '3949942 / ', '', 1, 1),
(316, '14783104', 'GERARDO JOSE', 'CONTRAMAESTRE LARA', '', '', '', 1, 1),
(317, '14784338', 'DIEGO', 'GRAJALES LOPEZ', '.', '', '', 1, 1),
(318, '14784812', 'LILIANA KATHERINE', 'ALVIAREZ DELGADO', '.', '', '', 1, 1),
(319, '14872042', 'GABRIEL', 'GARNICA COLMENARES', '.', '3423564', '', 1, 1),
(320, '14873324', 'MARIA VANESSA', 'MUJICA QUIÑONEZ', 'URB VILLA DEL EDUCADOR CALLE 3 Nº 87 VIA CHORRO DEL INDIO', '', '', 1, 1),
(321, '14873327', 'LUIS FERNANDO', 'RANGEL CRIADO', '.', '', '', 1, 1),
(322, '14873550', 'JUAN CARLOS', 'OSORIO MORANTES', 'Carrera 12 No. 10-41', '0276 3443057', '', 1, 1),
(323, '14942030', 'CARMEN YOLANDA', 'ROMAN MARIN', 'Urb. Villa Palermo Casa No. 11 San Benito', '0276-3411897', '04247849717', 1, 1),
(324, '14942332', 'GLENDA MILAGROS', 'DIAZ DEPABLOS', '', '', '', 1, 1),
(325, '15027226', 'EDUARDO ERNESTO', 'CASTELLANOS MUÑOZ', '.', '', '', 1, 1),
(326, '15437392', 'ROSA DE LA C.', 'QUINTERO CARRILLO', '23 DE ENERO PARTE BAJA BARRIO POZO AZUL CARRERA 3 CON CALLE 4 CASA Nº 4-108', '3471378', '0426 7765737', 1, 1),
(327, '15438070', 'NELSON LEONARDO', 'MEDINA LA CRUZ', '.', '', '', 1, 1),
(328, '15456635', 'WLADIMIR ISMAEL', 'CONTRERAS CARVAJAL', 'CALLE 11 ENTRE CARRERAS 21 Y 22, CASA NÂº 21-78', '', '', 1, 1),
(329, '15501581', 'ARMANDO', 'DELGADO MONTAÑEZ', '.', '', '', 1, 1),
(330, '15502665', 'YURAIMA ROCIO', 'CARRILLO LOZANO', 'URBANIZACION LOS ATICOS QUINTA MIS RETOÑOS LA CONCORDIA', '', '', 1, 1),
(331, '15503062', 'FREDDY ALEXANDER', 'PEREZ ZAMBRANO', '', '', '', 1, 1),
(332, '15503734', 'JUAN CARLOS', 'MORENO CASTILLO', '', '', '', 1, 1),
(333, '15565096', 'CRISTIAN OMAR', 'GONZALEZ MANTILLA', '.', '0276-3474250', '', 1, 1),
(334, '15565535', 'DULCE ANDREINA', 'BAUTISTA LOPEZ', '.', '0276-3553528', '', 1, 1),
(335, '15566106', 'DARLIX EUNICE', 'CHIRINOS FERNANDEZ', '', '', '', 1, 1),
(336, '15566556', 'JOSE ALEXANDER', 'VELAZCO SANCHEZ', 'carrera 4 entre calles 10 y 11 sector las golondrinas', '', '', 1, 1),
(337, '15566803', 'JOHANNA CAROLINA', 'CARCAMO MOLINA', '.', '', '', 1, 1),
(338, '15858980', 'JORGE ENRIQUE', 'CHACON SANCHEZ', 'CONJUNTO RESIDENCIAL DON LUIS BLOQUE TORBES APTO 4', '', '', 1, 1),
(339, '15988166', 'JHONFRY ADELSO', 'SALAS AVENDAÑO', '.', '0276-3531378', '', 1, 1),
(340, '16086955', 'IRIMAR DEL VALLE', 'MARQUEZ GONZALEZ', '.', '', '', 1, 1),
(341, '16124592', 'BREITNER HOSMAN', 'CONTRERAS GUERRERO', 'MADRE JUANA CALLE 1, VEREDA 4 # 4-60', '', '', 1, 1),
(342, '16232001', 'GERARDO JOSE', 'CAMARGO CHACON', 'Unidad Vecinal, Vereda 17, No. 7, La Concordia', '0276 3473661', '', 1, 1),
(343, '16401886', 'JENNIFER COROMOTO', 'ZAMBRANO CHACON', '.', '', '', 1, 1),
(344, '16408091', 'NELSON ENRIQUE', 'GELVEZ  NEIRA', 'La Playa, No. 2-91', '', '', 1, 1),
(345, '16611235', 'DARCY MARGARITA', 'GARAY GUERRERO', 'Av. Guayana, los Kioskos, Casa Nª ñ-72', '02763429240', '04164719896', 1, 1),
(346, '16778533', 'JUAN CARLOS', 'BECERRA SALAS', 'AV. PRINCIPAL MADRE JUANA, H-99', '0276-3433932', '', 1, 1),
(347, '16858094', 'ISMAEL LEONARDO', 'CARDOZO VERANO', 'El Tambo, Via Santa Ana, Vedreda 1 Casa s/n', '', '', 1, 1),
(348, '16982712', 'VICTOR MANUEL', 'SANCHEZ CARDONA', 'pasaje cumanacoa puente real', '5147164', '04161378405', 1, 1),
(349, '17108248', 'ROS MILEYDI', 'MONCADA LOZANO', 'Barrio San Jose, de la Guayana, calle 5 Casa No. 2-68', '0276-3414259', '', 1, 1),
(350, '17108804', 'ARCELIA BEATRIZ', 'CORONADO CABANZO', 'Av. Ferrero Tamayo', '0276-3554604', '', 1, 1),
(351, '17501106', 'LUIS ASDRUBAL', 'LEAL VANEGAS', '.', '', '', 1, 1),
(352, '17501149', 'ROSANA KARINA', 'RAMIREZ PULIDO ', 'conjunto residencial los umuquenas, torre 11 piso 2 apto. 11-24', '0276-3442023', '0414-7091355', 1, 1),
(353, '17503749', 'MIGUEL ANGEL', 'MENDEZ SIERRA', '', '', '', 1, 1),
(354, '17932848', 'JEFFERSON FERNANDO', 'OCHOA VIVAS', 'Santa Ana del  tachira, calle 1, casa No. 06', '', '', 1, 1),
(355, '18089978', 'MARIA CRISTINA', 'RODRIGUEZ PULIDO', '', '', '', 1, 1),
(356, '18256463', 'FRANKLIN ALONSO', 'SANCHEZ CASTELLANOS', '.', '', '', 1, 1),
(357, '19133525', 'HUGO ALBERTO', 'BONILLA BONILLA', 'CARRETERA RUBIO KM3, PAN DE AZUCAR CASA S/N', '', '', 1, 1),
(358, '19358143', 'ROSARY YOLI', 'AYALA MOLINA', 'Tariba, MonseÃ±or BriceceÃ±o, calle 11, Casa No7-37', '0276-3947280', '', 1, 1),
(359, '19596201', 'JOSE BLADIMIR', 'CASTRO ZAMBRANO', '.', '', '', 1, 1),
(360, '19598603', 'JEFFERSON OMAR', 'ANGOLA VARELA', '.', '', '', 1, 1),
(361, '22662384', 'JORGE', 'MENESES CONTRERAS', '', '', '', 1, 1),
(362, '24328628', 'YASMIN ', 'ROSAS DIAZ', '.', '0276-3576043', '0426-5701425', 1, 1),
(363, '8510801', 'JOSE OCARIS', 'CARDENAS TORRES', 'CASA Nª 98', '', '04141771769', 1, 1),
(364, '15080304', 'JACKSON HUMBERTO', 'ROMERO TREJO', 'CALLE 6 ENTRE CARRERAS 1 Y 2 CASA Nº 1-23', '', '04269266283', 1, 1),
(365, '13999647', 'EBY ALEXANDER', 'APONTE LEAL', 'PAN DE AZUCAR VIA RUBIO', '', '04165749515', 1, 1),
(366, '7834676', 'PEDRO SENEVIER', 'GOMEZ LUNA', 'PIRINEOS 1 LOTE A CASA Nº 3', '3534462', '04162731429', 1, 1),
(367, '13505824', 'FRANKLIN EDUARDO', 'RODRIGUEZ CASTAÑEDA', 'BLOQUE 7 PISO 7 APARTAMENTO 07-01', '3479707', '04147020519', 1, 1),
(368, '17501735', 'JAVIER ALEXIS', 'DUARTE BELLO', 'AVENIDA LUCIO OQUENDO VEREDA 2 Nº C-76', '5173492', '04247371421', 1, 1),
(369, '5653617', 'DORANGEL', 'VIVAS MORA', 'CALLE ESTADIUM Nº 1-16 EL ABEJAL DE PALMIRA', '', '', 1, 1),
(370, '16983599', 'PABLO EMILIO', 'SANCHEZ ORTIZ', 'CALLE 8 CASA Nº 2-13', '', '04141562169', 1, 1),
(371, '16982506', 'JHOEL FRANCISCO', 'RIOS', 'CARRERA 3 CASA Nª 9-25', '8084595', '04247287885', 1, 1),
(372, '13304380', 'BELMAN', 'MARIÑO MARIÑO', 'LAS VEGAS DE TARIBA CARRERA 5 BIS CASA N 1-110', '', '', 1, 1),
(373, '18880302', 'YORGER ALEJANDRO', 'OCHOA', 'SABANETA CALLE PRINCIPAL CRISTOBAL MENDOZA CASA N. 00-85', '', '04247213918', 1, 1),
(374, '16408953', 'NIXON MIGUEL', 'GUERRERO CACERES', ' RESIDENCIAS PISO 4 APTO 01-04', '04147157814', '', 1, 1),
(375, '5659455', 'MIGUEL ANGEL', 'TIBAMOSO TORRES', 'CALLE 12 ENTRE PASAJE CUMANA Y GUASDUALITO PUENTE REAL Nª G-22', '', '', 1, 1),
(376, '19135289', 'CHARLES', 'ARMAS NORIEGA', 'CALLE 2 Nº 3-30 LA POPITA', '04147104320', '', 1, 1),
(377, '18879039', 'MANUEL ALFONSO', 'COLMENARES CORTESIA', 'VEGA DE LA TINTA FINCA LA ESMERALDA CARRETERA VIEJA DE SABANETA', '04121719482', '', 1, 1),
(378, '17644928', 'JACKSON EDUARDO', 'BECERRA', 'PUENTE REAL PASAJE GUASDUALITO CALLE 9 Y 10 Nº 9-58', '', '', 1, 1),
(379, '18353677', 'AURA GLORIS', 'VIVAS DELGADO', 'CARRERA 18, Nº 16-26, ', '0276-3569947', '04147071582', 1, 1),
(380, '11113980', 'YELENA INES', 'VARELA RAMÍREZ', 'PUEBLO NUEVO', '', '04147061619', 1, 1),
(381, '12229550', 'KARINA ELIZABETH', 'DÍAZ DEPABLOS', 'AVENIDA LOS KIOSCOS CASA  Nº 0-208', '0276 - 4216115', '0414 - 2783138', 1, 1),
(382, '12630587', 'LUIS EDUARDO', 'MEDINA GALLANTI', 'AVENIDA PPAL DE PUEBLO NUEVO, RESD. LOS NARANJOS TORREA P5 APTO 504', '3421047', '04147075560', 1, 1),
(383, '14941852', 'ZAIRA JANETH', 'VALERO BERBESI', 'EDIFICIO ALTAMIRA PISO 2 APARTAMENTO 2-3', '0276-3478842', '0424-7267696', 1, 1),
(384, '18990019', 'ANDREA VANESSA', 'GARCIA CARRERO', 'SAN CRISTOBAL', '', '', 1, 1),
(385, '20077488', 'ADRIANA COROMOTO', 'ZAPATA SANCHEZ', 'JUNCO VIEJO  SECTOR BELLA VISTA', '', '04247422924', 1, 1),
(386, '13037901', 'PAUL ANDREY', 'ZAMUDIO VALERO', 'PIRINEOS NORTE EDIFICIO 01 APTO 01-01', '0276-4124285', '0426-3294593', 1, 1),
(387, '14264382', 'JOHNNY ARCANGEL', 'MANCHEGO DELGADO', 'PASAJE GUASDUALITO ENTRE CALLES 13 Y 14 NRO CIVICO 13-53', '0276-3410039', '0424-7351867', 1, 1),
(388, '17502701', 'YERLYN ZULMARY', 'GARCIA RAMIREZ ', 'RESIDENCIAS LA FLORIDA EDIFICIO FRAILEJON PISO 5 APTO A-5', '0276-3477828', '', 1, 1),
(389, '19360412', 'DUDBER ALFREDO', 'HERNANDEZ VILLAMIZAR', 'SAN JOSECITO SECTOR LOS ANDES CALLE 6 CASA 3-28', '', '0424-7254481', 1, 1),
(390, '19522033', 'ANGELO OSWALDO', 'CAICEDO CEGARRA', 'URB EL DIAMANTE II CARR 2 NRO 4-13', '0276-3947545', '0416-0775916', 1, 1),
(391, '15231452', 'ELISETH PERLEY', 'MARTINEZ VILLAMIZAR', 'CARR 4 CALLE 1 URB PIÑAR DEL BOSQUE NRO 3-26 ', '0276-3946041', '0426-7758823', 1, 1),
(392, '15233688', 'JESÚS ONESIMO', 'CONTRERAS ROJAS', 'SAN RAFAEL VEREDA 6 NRO 0-42', '', '0414-7186142', 1, 1),
(393, '19776794', 'KEYLA YESENIA', 'VERGEL PAEZ ', 'TARIBA BARRIO MONSEÑOR BRICEÑO CALLE 10 CON CARRERA 10', '', '0426-8284667', 1, 1),
(394, '18880510', 'ELBA MARIA', 'MUÑOZ MENDEZ', 'PASAJE GUASDUALITO ENTRE CALLE 13 Y 14 #13-53 PTE REAL ', '', '0424-7284900', 1, 1),
(395, '17644149', 'YEISON ALFREDO', 'ROJAS BELLO', 'CARRERA 6 TARIBA Nº 8-40', '0276-3913740', '0416-7794711', 1, 1),
(396, '17812544', 'MAYRA ALEJANDRA', 'ESTUPÍÑAN', 'SAN JOSECITO; SECTOR A, VEREDA EL SAMAN', '', '0426-9828465', 1, 1),
(397, '12233901', 'IRIS YORLEY', 'NEIRA DE MONTAÑEZ', 'BARRIO EL RIO, RAFAEL MORENO', '02764176748', '04262111591', 1, 1),
(398, '19665611', 'CIELO KARINA', 'SUAREZ BOADA', 'CALLE PRINCIPAL LA ADUANA, VEREDA SUCRE CASA Nº 03', '02763947829', '04162734571', 1, 1),
(399, '11491651', 'JOSE LEONARDO', 'RODRIGUEZ CONTRERAS ', 'CALLE 11 LA ERMITA ENTRE CARRERA 1 Y PASAJE CUMANA', '', '0426-7272809', 1, 1),
(400, '9229655', 'LUCY JEANNETTE', 'URBINA MENDEZ ', 'CALLE 1 URB. ANTONIO JOSÉ DE SUCRE; CASA Nª 32, SAN CRISTÓBAL, ESTADO TACHIRA', '02763480472', '04147085889', 1, 1),
(401, '18353344', 'WILLIAM EDUARDO', 'DAZA DIAZ ', 'CAPACHO LIBERTAD BARRIO CENTENARIO, CALLE 1, CASA Nª 5', '0276/7880711', '0414/7188899', 1, 1),
(402, '16421744', 'FRANCISCO JAVIER', 'ARDILA SANCHEZ', 'san felix municipio ayacucho aldea el carmen via la fria', '0426/8766575', '0424/7625439', 1, 1),
(403, '18391763', 'DULCE NATHALY', 'ZAMBRANO CHAPARRO', 'PIRINEOS', '0276-3553479', '0414-7127206', 1, 1),
(404, '16124541', 'DECSY LISSET', 'BRICEÑO DELGADO', 'LA CONCORDIA CRRA 4 CON CLL 5 CASA 4-188', '', '0414-7122426; 0424-7', 1, 1),
(405, '10177251', 'JOSÉ LEÓN', 'PERNIA PEÑARANDA', 'CALLE PRINCIPAL DE TOIQUITO, VEREDA 07 NºV13. PALMIRA', '0426-4771984', '', 1, 1),
(406, '17503823', 'YNDIRA MERCEDES', 'HURTADO ROSALES', 'LA ERMITA CLL 15 ENTRE CRRA 1 Y PASAJE CUMANA', '0276-3417140', '0416-8767085', 1, 1),
(407, '21001533', 'NOHELIA CAROLINA', 'ROSALES FLORIDO', 'LA LAGUNA. PALMIRA, CALLE LAS FLORES CASA 0-28', '0276-3945981/0426-25', '0424-7174022', 1, 1),
(408, '19977297', 'MAITE CAROLINA', 'SILVA HERNANDEZ ', 'BARRIO EL RIO SECTOR RAFAEL MORENO PARTE BAJA 18', '', '0416-5794577', 1, 1),
(409, '13146465', 'RONALD ALEXANDER', 'LARA ALBARRAN', 'SAN CRISTOBAL', '', '0426-6273246', 1, 1),
(410, '19977459', 'DELBIN LEONEL', 'RAMIREZ COLMENARES', 'CALLE 2 CON CARRERA Nº 2-02 PRADOS DEL TORBES TARIBA', '0276-8835493', '0424-7785443', 1, 1),
(411, '16409922', 'FREDDY ORLANDO', 'BELTRAN CANCHICA', 'AV ROTARIA  CASA 64-79', '0276-3463758', '0414-7378006', 1, 1),
(412, '13821180', 'JECKSON YOVANNY', 'GOMEZ GUEVARA', 'SAN CRISTOBAL ', '', '0412-1630826; 0412-6', 1, 1),
(413, '19353888', 'JOSE ABEL', 'BUITRAGO CARRILLO', 'TUCAPE VIA PRINCIPAL SECTOR LOS PINOS CASA Nº 3-82', '0276-6518304', '0414-7512109', 1, 1),
(414, '16958778', 'JEFERSON ALBERTO', 'SANDOVAL', 'TUCAPE SECTOR LOS PINOS VIA PRINCIPAL PARTE BAJA 3-85', '', '0414-7042115', 1, 1),
(415, '14450617', 'ERIK REINALDO', 'SANCHEZ LEON ', 'AV. LICIO OQUENDO, EDIF: LA CONSOLACION, PISO 1, APTP. Nº 2, LA CONCORDIA ', '0276-4147769', '0424-7113354', 1, 1),
(416, '14873792', 'JARRY DAVID', 'DAVILA LEAL', 'CALL 02 VEREDA 8 CASA 179', '', '0416-9714230', 1, 1),
(417, '17931318', 'WOLFREDO JOSE', 'MARQUEZ RONDON', 'BARRIO OBRERO CLL 13 NUM 13-15', '0276-3438670', '0424-7242077', 1, 1),
(418, '16778956', 'PEDRO LUIS', 'ZAMBRANO CONTRERAS', 'CORDERO CARRERA 1 CON CALLE 1 LLANITOS APTO .1', '0426-4267410', '0426-7755343', 1, 1),
(419, '19877119', 'ANTONY BEXANDER', 'CAMARGO MUÑOZ', 'SANTA ANA ', '', '0416-3886323; 0416-1', 1, 1),
(420, '17770649', 'WILMER ALI', 'BENITEZ MORENO', 'PALMAR VIEJO TERRAZA NÂº 1 VEREDA 17 casa NÂº 95', '02763436539', '04247507543', 1, 1),
(421, '13304424', 'HEBER YOVANNY', 'SANCHEZ NIETO', 'KM 3 PAN DE AZUCAR VIA RUBIO ', '0276-3411733', '04261374563', 1, 1),
(422, '15990130', 'BRAY NATHANIEL', 'BERRIOS PERNIA', 'LAS VEGAS DE TARIBA', '0276-3948836', '0416-6024284', 1, 1),
(423, '12234999', 'NELSON OSMAR', 'PINEDA RONDON', 'TORRE B PISO 01 Apto 1-06', '0276/3431312', '0424/2690828', 1, 1),
(424, '17646441', 'MARIA DANIELA', 'VELASCO USECHE', 'RESIDENCIAS MIURA, TORRE B, PISO 3 Apto 3-H', '0276/3532813', '0414/0755186', 1, 1),
(425, '3795288', 'HUGO ENRIQUE', 'MACABEO PABON ', 'CALLE 3 Nº 1-83 LA CONCORDIA', '0276/3468196', '0414/7107344', 1, 1),
(426, '18565599', 'MARIA LOURDES', 'SERRANO MOLINA', 'ZORCA PROVIDENCIA, VIA LAS MARGARITAS DE TARIBA, SECTOR EL PROGRESO, VEREDA LA SERRANIA PRIMERA CASA', '0276/3822094', '0414/0786963', 1, 1);
INSERT INTO `funcionarios` (`ide_fun`, `ced_fun`, `nom_fun`, `ape_fun`, `dir_fun`, `tel_fun`, `cel_fun`, `ide_par`, `sta_fun`) VALUES
(427, '13550136', 'NARADA EDUVIGES', 'CACERES PEREZ', 'BARRIO GUZMAN BLANCO CALLE 1 ENTRE CARRERAS 8 y 9 Nº 8-40 ', '0276/3419934', '0414/2993394', 1, 1),
(428, '17677856', 'PEDRO PABLO', 'CHACON DIAZ ', 'EL ABEJAL DE PALMIRA VEREDA 5 CASA Nº 4-34', '0276/3941545-0276/39', '0426/3266510-0424/70', 1, 1),
(429, '10167259', 'JHONNY ORLANDO', 'RUIZ LOPEZ', 'CALLE 2 CON CARRERA 2 PASAJE ALTAMIRA. LA ERMITA ', '', '0426/3423124-0416/67', 1, 1),
(430, '17503460', 'FREITER MEDARDO', 'MEJIA SERNA ', 'EL LLANITO VIA CAPACHO SECTOR EL CAMINO REAL CALLE 1RO DE MAYO ', '', '04143796035', 1, 1),
(431, '16231903', 'JAIME WILFREDO', 'OTERO SILVA ', 'CARRERA 10 Nº 7-47 BARRIO EL CARMEN. LA CONCORDIA ', '0276/3553302', '0426/8732802', 1, 1),
(432, '9207191', 'DANIEL', 'CASTELLANOS VALENCIA ', 'UNIDAD VECINAL VEREDA 2 CASA Nº 7', '0276/3466215', '0424/1462164', 1, 1),
(433, '14349472', 'YESNARDO JOSE', 'CANAL MENDOZA', 'LA CONCORDIA ', '', '', 1, 1),
(434, '18379754', 'JUAN ALBERTO', 'AREIZA SERRANO ', 'AV. PRINCIPAL DE PUEBLO NUEVO CON AV GUAYANA ', '0276-3431739', '0416-3706329; 0424-7', 1, 1),
(435, '16229275', 'MAYRA ALEJANDRA', 'ARGUELLO', 'RIBERAS DEL TORBES CALLE 5 NRO. 60', '', '0424-7371028', 1, 1),
(436, '12970662', 'FRANKLIN ALBERTO', 'CHACON ROPERO', 'LA CONCORDIA', '0276/3940465', '0416/6773441', 1, 1),
(437, '16541246', 'ANDRES ELOY', 'MENDOZA URREA', 'BARRIO BOLIVAR VEREDA 2 EL BOSQUE CASA NRO. 6-38', '0276-3412785', '0414-7135558; 0412-4', 1, 1),
(438, '15231097', 'LUIS RODOLFO', 'CACERES DUARTE ', 'EL LLANITO VIA CAPACHO CALLE 1RO DE MAYO ', '', '0416-7968331', 1, 1),
(439, '16612290', 'JEYMAR MANUEL', 'LUNA PEÑARANDA', 'LA CONCORDIA', '', '', 1, 1),
(440, '15566860', 'SAMUEL DAVID ', 'ZAMBRANO', 'LA CONCORDIA ', '', '', 1, 1),
(441, '14707543', 'JOSE ALFREDO', 'ANGARITA FUENTES', 'BARRIO CEMENTERIO CALLE PRINCIPAL DIAGONAL A LA CANCHA TECHADA LIBERTAD CAPACHO VIEJO', '', '', 1, 1),
(442, '17370950', 'FRANQUIN OVIDIO', 'CONTRERAS CACERES', 'CARRERA 11 ENTRE CALLES 9 Y 10 Nº 9-38 TARIBA', '0276/8731916', '0414/9794335', 1, 1),
(443, '19358909', 'JOSE DANIEL', 'CAICEDO LUGO', 'CALLE 1 VEREDA NRO 1-44 ', '0276-3474324', '0424-7045856', 1, 1),
(444, '14504649', 'GLENDA DEL CARMEN ', 'GONZALEZ IBAÑEZ ', 'CALLE PRINCIPAL GENARO MENDEZ CASA 7-124', '', '0426-6767012; 0416-6', 1, 1),
(445, '16744739', 'ANA BENILDE', 'CHACON CHACON', 'VIA PRINCIPAL CASA DEL PADRE SECTOR SANTA FILOMENA', '0276-8835870', '0426-7275595', 1, 1),
(446, '17828591', 'LUIS EDUARDO', 'VELAZQUEZ SOSA', 'SAN CRISTOBAL', '', '', 1, 1),
(447, '17812812', 'INGRID MILEIDES', 'ZAMBRANO CRIOLLO', 'URB ANDRES BELLO CALLE 6 CASA Nº 6-26', '02763961851', '04145809850', 1, 1),
(448, '18256232', 'NATHALY ', 'GARNICA COLMENARES', 'CALLE N. 8 CASA 8-30', '02763916210', '04149755938', 1, 1),
(449, '18791284', 'SHEILA JOHELY', 'PANTALEON FIGUEROA', 'Barrancas, Parte Alta, Calle El Mirador, No. 4-40', '0276/3916952', '0414/7182520 - 0414/', 1, 1),
(450, '15503192', 'DORMERELLY', 'BECERRA ROSALES', 'ALTOS DE PARAMILLO / PARCELA 02 CASA NRO 8', '0276/3572433', '04147372623', 1, 1),
(451, '20099696', 'YOHANDY JOSE ', 'PEREZ GONZALEZ', 'CALLE 2, SANTA TERESA ', '0276/5165460', '', 1, 1),
(452, '17109886', 'DAIZ NAKARY', 'CONTRERAS', 'CARRERA 15, BARRIO OBRERO ', '', '', 1, 1),
(453, '19541393', 'YOLMAR NICOLAS', 'CHACON RANGEL', 'CALLE 2 CASA SN SANTA ANA', '', '0424-7235584', 1, 1),
(454, '16983269', 'ESTHER ALEJANDRA', 'ORTEGA CARDENAS ', 'PIRINEOS I LOTE G VEREDA 5 CASA #08', '0276-3554737', '0424-7720611', 1, 1),
(455, '13892275', 'REYMER ALAIN', 'TARAZONA ZAMBRANO ', 'BARRIO SAN JOSE VEREDA 1, SECTOR LA POPITA, PUEBLO NUEVO, NRO 0-115', '0424-7200794', '0424-7200794', 1, 1),
(456, '19918907', 'REYES  ', 'MARIA ELENA', 'BARRIO BOLIVAR', '', '', 1, 1),
(457, '21417627', 'GRETEL SARANYEL', 'CHACON SANCHEZ ', 'SANTA ANA URB  EL CARRIZAL CALLE 2 CASA Nº 95', '', '', 1, 1),
(458, '15085444', 'EVERT JOSE', 'BORRERO CHACON', 'CALLE COLINAS DE CARABOBO CASA N°2-23 SAN CRISTOBAL', '', '', 1, 1),
(459, '16745686', 'KERYN MORELLA', 'RANGEL HUIZA', 'URB. VILLA BOLIVAR, SECTOR GUARAMITO, CASA No. 4', '0277-3749551', '0414-7002971', 1, 1),
(460, '19502568', 'DIANA ELISA', 'TORRES VEGA', 'CALLE 15 CARRERA 14 Nº 14-17 BARRIO OBRERO', '0276-4249222', '0414-7589566', 1, 1),
(461, '17492445', 'CESAR ALFREDO', 'ROMERO BECERRA', 'San Cristobal', '', '', 1, 1),
(462, '3790680', 'ANA LUISA', 'RAMIREZ DE ACOSTA', 'LA GUACARA, ENTRE CALLES 5 Y 6, PASAJE MUCURITA, CASA No. 5-27', '', '', 1, 1),
(463, '28304334', 'STIVENT HERNANDO', 'POVEDA PULIDO', 'CAPACHO', '', '', 1, 1),
(464, '18392840', 'DENISSE JOSEFINA', 'MORA PAEZ', 'URB. LAS ACASIAS, CARRERA 4, No. 1-53', '0276/3561834', '', 1, 1),
(465, '16230394', 'BEISY THAMARA', 'RAMIREZ VIVAS', 'BLOQUE B-3, PISO 3, APTO. 34, TACHIRA', '0276/3438322 ', '0416-4785426', 1, 1),
(466, '17503981', 'JOHAN MANUEL', 'SUAREZ DUQUE', 'URB LA VEGA CALLE Nª6 CASA Nª6-67', '02764229047', '0426-3511329', 1, 1),
(467, '20425038', 'NELSON ALBERTO', 'SUAREZ MARTINEZ', 'BARRIO SAN CARLOS CARRERA 13 CALLE 9 Y 10', '02763432927', '04265771987', 1, 1),
(468, '17208202', 'ALBERTO ANTONIO', 'PORRAS', 'BARRIO LAS MARGARITAS VEREDA 3 PARTE BAJA CASA 30', ' 02766112780', '04147033674', 1, 1),
(469, '19502580', 'ALVARADO USECHE', 'MIGUEL EDUARDO ', 'CALLE 13 ENTRE PASAJES CUMANA Y GUASDUALITO Nª DE CASA G-44', '0276-34220275', '0414-7541611', 1, 1),
(470, '23134065', 'NIXON GABRIEL ', 'BORJA CARDENAS', 'CENTRO CARRERA 9 ENTRE CALLES 12 Y 13, CASA Nª 12-51 ', '', '0424-7793227', 1, 1),
(471, '21180312', 'YUDARY NILEIDY', 'SALCEDO GARCIA ', 'COLINAS DE TOITUNA MUNICIPIO GUASIMOS CALLE 2 CASA S/N', '', '04247103824', 1, 1),
(472, '20517714', 'RICARDO JOSE', 'CARBALLO VELASCO ', 'URB COLINAS DEL AURORA MANZANA D-1 SABANETA MUNC CARDENAS', '', '0416-2785045', 1, 1),
(473, '19135631', 'MARIA ELENA', 'MONROY GARCIA ', 'PALMAR RAMIREÑA SECTOR LAS MERCEDES EL TAMBO - MUNICIPIO CORDOBA', '', '0416-6706146', 1, 1),
(474, '21086178', 'JOSE ALEJANDRO', 'GAMEZ FIGUEROA', 'KM 7 VIA RUBIO SECTOR EL PORVENIR CALLE PRINCIPAL CASA B-49', '', '0416-2749491', 1, 1),
(475, '21220997', 'JEFFERSON IVAN', 'RAMIREZ ALVAREZ ', 'RUBIO SECTOR LA QUIRACHA BLOQUE 29 APARTAMENTO 03-03', '', '0416-0345084', 1, 1),
(476, '25314479', 'NELLY YEXUNET', 'GUERRERO SANCHEZ', 'AVENIDA PRICIPAL AGUA DULCE MUNICIPIO TORBES ', '', '0416-1105289', 1, 1),
(477, '25977204', 'WILMER ORLANDO', 'MARQUEZ LOPEZ ', 'UBR CAFETAL 2 CASA Nº 141 SANTA ANA', '', '0414-7436128', 1, 1),
(478, '23136842', 'MARIA GABRIELA', 'CHAVEZ CORZO', 'URB RINCON DE LA VEGA CARRERA 3 CON CALLE 3 Y 4 CASA Nº UR-20893', '0276-3488694', '0426-1776800', 1, 1),
(479, '22675724', 'WANDA YORYELY', 'USECHE SUAREZ', 'CALLE 15 PASAJE GUASDUALITO CASA Nº 15-6', '0276-3423976', '0414-9721410', 1, 1),
(480, '19585917', 'YENNIRE KATIUSKA', 'YEPEZ GUTIERREZ', 'EL VALLE CURVA EL AGUILA CASA Nª 6 MUNICIPIO INDEPENCIA ', '0276-3488077', '0416-1733683', 1, 1),
(481, '19541182', 'HERIBERTO JOSE', 'COLMENARES  BASTIDAS', 'CARRERA 8 CON CALLE 15 Y 16 CAS Nª 15-65 BARRIO LIBERTADOR ', '0276-4153073', '0416-0766105', 1, 1),
(482, '19926619', 'DANNY OSCAR', 'CASIQUE JAIMES', 'EL PUEBLITO VIA RUBIO ', '', '', 1, 1),
(483, '19491189', 'NELLY JACKELINE', 'RAMIREZ ROA', 'URB VEGA DE AZA PRIMERA ETAPA CALLE 5 CASA Nº 36', '0276-4123990', '0424-7013005', 1, 1),
(484, '25889533', 'JEAN CARLOS', 'MOROS BLANCOS ', 'EL TOPE KM 5 VIA RUBIO VEREDA LA HACIENDA  CASA SIN N', '0276-6515214', '0426-8035107', 1, 1),
(485, '25837486', 'JHON ALEXANDER', 'MORILLO HERNADEZ', 'BARRIO LOS NAZARENOS SANTA ANA ', '0276-3576229', '0414-7191710', 1, 1),
(486, '20122546', 'MARYORI KARINA', 'RAMIREZ DELGADO ', 'SAN JOSECITO SECTOR D PEDRO UMBERTO DUQUE CALLE PRICIPAL CASA Nª50', '', '0414-3746675', 1, 1),
(487, '18798808', 'ANKRY MAXORY', 'ANGULO RONDON ', 'URB FRANCISCO JAVIER CALLE 11 ', '', '0414-7114865', 1, 1),
(488, '25166603', 'CRISTHIAM JOSUE ', 'BRACAMONTES BASTOS ', 'EL VALLE VIA CAPACHO SECTOR EL BOLON UBR TERRAZAS DEL BOLON CASA Nª 4', '0276-8833753', '', 1, 1),
(489, '25639521', 'DEDIER LEANDRO ', 'MACHADO AMAYA ', 'CALLE 2 SECTOR ESTACION SANTA EDUVIGES SANTA ANA ', '', '', 1, 1),
(490, '24743862', 'EMIGDIO ARCANGEL ', 'PARADA HUERFANO ', 'CARRETERA VIA RUBIO KM 5 SECTOR EL PUEBLITO CASA S/N PARROQUIA MANUEL FELIPE RUGELES', '', '0416-0720110', 1, 1),
(491, '24777959', 'JOSE YOUSEPHE', 'CHACON ARBOLEDA', 'EL PIÑAL CALLE 2 CON CARRERA 2 CASA Nª2-23', '', '0416-2757439', 1, 1),
(492, '23826400', 'YARDELYN', 'VILLAMIZAR RUIZ ', 'CAPACHO LIBERTAD SECTOR LA COLINA CALLE PAEZ CASA 1p-65', '0276-7880616', '0414-0782227', 1, 1),
(493, '18762506', 'JACKSON JAVIER', 'URBINA VILLAMIZAR ', 'BARRIO LAS MERCEDES CALLE 11 CON CARRERA 8 CASA Nª8-28', '0276-8088770', '0416-1173383', 1, 1),
(494, '21086811', 'DEIBER JAVIER ', 'LEAL JIMENEZ', 'POZO AZUL - RUBIO ', '', '0426-9794545', 1, 1),
(495, '17107561', 'GREYNER SIMON', 'MARQUEZ CARO', 'SAN JOSECITO SECTOR 1 VEREDA 19 CASA Nº 12 PARTE BAJA', '', '0426-6394218', 1, 1),
(496, '21341528', 'YETSSI KATHERINE', 'RAMIREZ VARGAS ', 'NARANJALES CALLE 2 MANZANA 007 P038', '', '', 1, 1),
(497, '19360804', 'DARWIN MANUEL ', 'DOS SANTOS VANEGAS', '23 DE ENERO PARTE ALTA CALLE 7 CON CARRERA 3 CASA Nª2-49 LA CONCORDIA', '02763467262', '0424-4449682', 1, 1),
(498, '24775327', 'JHONKLEYNNER MOISSES', 'ALBARRAN CASANOVA', 'Barrio Obrero carrera 18 casa Nª15-28', '0276-3534021', '0414-7280122', 1, 1),
(499, '24745214', 'LILIANA NAYRET', 'BAUTISTA CAICEDO', 'SAN CRISTOBAL ', '', '', 1, 1),
(500, '21418101', 'BERNARDO ENRIQUE', 'CONTRERAS PEÑA', 'SAN CRISTOBAL', '', '', 1, 1),
(501, '18860986', 'CARMEN IRENE', 'GOMEZ', 'Sector G, Calle Principal No. 2-84, San Josecito', '0424/2921494', '0416/3755421', 1, 1),
(502, '23828162', 'YEFFERSON RAFAEL ', 'MORA MOLINA ', 'PASAJE ACUADUCTO CASA 24-36', '', '', 1, 1),
(503, '19134969', 'DANIEL ANTONIO', 'MARTINEZ RANGEL', '23 DE ENERO CALLE 2 CASA Nª 2-131 PARTE BAJA', '02763463473', '04147516781', 1, 1),
(504, '21085372', 'ENYER DAVID', 'FAJARDO RODRIGUEZ ', 'CARRERA 6 CASA Nº 0-.4 AMBROSIO PLAZA ', '', '', 1, 1),
(505, '16778197', 'JOHANNY JOSE', 'NIETO MORALES', 'SAN CRISTOBAL ESTADO TACHIRA', '', '', 1, 1),
(506, '16333970', 'KAREN MERCEDES', 'ZAMBRANO GARCIA', 'BARRIO LAS FLORES CALLE PRINCIPAL Nº 2-50', '02763467790', '0424-7526599', 1, 1),
(507, '12580196', 'JULIO CESAR', 'PADRON RAMIREZ', 'LAS VEGAS DE TARIBA ', '', '0412-6567455', 1, 1),
(508, '19878557', 'ISLEY GERALDIN', 'FUENTES DAZA ', 'CALLE 3 SECTOR SAN BENITO    PALMIRA', '0276-6519436', '0424-7661133', 1, 1),
(509, '25713047', 'JESUS DAVID', 'HERNANDEZ MONSALVE', 'DIAMANTE II VEREDA 6 CASA N°20 ', '0276-4151336', '0416-1776363', 1, 1),
(510, '17207705', 'LUBRIALY  BRIGGITE', 'LEAL ALVIAREZ', 'LAS VEGAS DE TARIBA RESIDENCIA DON LUIS EDIF URIBANTE APTO 03', '', '', 1, 1),
(511, '17368533', 'EFRAIN CIRILO', 'BUENO LOPEZ', 'BARRIO COLON CALLE 2 CASA N°1-83 SECTOR LA GUAYANA', '0276-4150721', '0424-7256939', 1, 1),
(512, '19236334', 'WILBERTT ALEJANDRO', 'MANZANILLA MONCADA', 'CALLE 7 CASA 3-17', '0276-3919639', '0416-6764792', 1, 1),
(513, '19033282', 'RENZO JOSEPH', 'CARDENAS CARRILLO', 'CALLE PRINCIPAL BARRIO BOLIVAR QTA 13', '02764140655', '04246370255', 1, 1),
(514, '16320846', 'HECTOR ENRIQUE', 'ANGULO CHACON', 'CALLE LOS ALMENDROS CASA C-13', '02763538275', '04164056142', 1, 1),
(515, '16779607', 'ISIS ADONIA', 'COLMENARES LOZANO', 'COREDERO LOMAS BLANCAS URB.5 CASA 41', '04147489179', '', 1, 1),
(516, '19599840', 'OMAR ANTONIO', 'SAYAGO COLMENARES', 'CARRERA 1 NRO. 3-96', '02763563882', '04140806354', 1, 1),
(517, '19522287', 'VLADIMIR TEODORO', 'ACEVEDO UZCATEGUI', 'CALLE 14 ENTRE CARRERAS 8 Y 9 CASA NRO. 8-36', '', '04143769155', 1, 1),
(518, '21221203', 'DANIELA NATHALY', 'JAIMES SANTANDER', 'CALLE 6 CENTRO DE SAN CRISTOBAL', '', '04126824410', 1, 1),
(519, '16778798', 'LORENA', 'GUERRERO JAIMES', 'CALLE SUCRE PARTE ALTA CASA NRO 4-03', '04247271400', '04247073431', 1, 1),
(520, '16779815', 'JOSE JAVIER', 'HUERFANO SANCHEZ', 'AV. CARABOBO CALLE 10 CASA 17-24', '02763433479', '04247149758', 1, 1),
(521, '13149384', 'DARCY NAKARY', 'SANCHEZ DE CACERES', 'AV.UNIVERSIDAD CALLE PRINCIPAL CASA 4-65', '02763563621', '041687711609', 1, 1),
(522, '17862619', 'GERARDO ALEXIS', 'ACEVEDO UZCATEGUI', 'CALLE 14 ENTRE CARRERA 8 Y 9', '', '027647396064', 1, 1),
(523, '25703619', 'DANNY ISAAC', 'MORENO RAMIREZ', 'TORRE 1 PISO 7 APTO 171', '', '04147539491', 1, 1),
(524, '24745419', 'JESUS ENRIQUE', 'MONTERO COLMENARES', 'ABEJAL SECTOR LA PEDREGOSA CASA 0-11', '', '04262650185', 1, 1),
(525, '18969381', 'MARIAM LORAGNY', 'CHACON GIL', 'SAN CRISTOBAL', '02767885483', '04147216454', 1, 1),
(526, '21220294', 'LUISANA', 'PERNIA PEÑA', 'ROMULO GALLEGOS VEREDA 2', '', '04149714742', 1, 1),
(527, '15156890', 'ERIC DARIO', 'CARDENAS ESPINOZA', 'CALLE VENEZUELA CASA NRO. V-24  ZORCA', '04124283585', '04161373920', 1, 1),
(528, '17206745', 'DANNY ENRIQUE', 'PORTILLO VIVAS', 'SAN CRISTOBAL', '', '', 1, 1),
(529, '15080858', 'NELSON ALBERTO', 'TARAZONA GONZALEZ', 'SAN CRISTOBAL', '', '', 1, 1),
(530, '9337977', 'ROSA ANGELINA', 'HERNANDEZ TORO', 'ALTOS DE PARAMILLO URB LAS MANZANAS CALLE LOS CEDROS', '', '', 1, 1),
(531, '13506917', 'YAMILE MILDREY', 'PARADA CHACON', 'CALLE 5 CASA S-65 PALMIRA', '', '', 1, 1),
(532, '15763461', 'RAMON ENRIQUE ', 'BUCOTT HERNANDEZ', 'CALLE 2 CASA Nª15', '0276-3465826', '0426-8565773', 1, 1),
(533, '4629005', 'SALIM ANTONIO', 'BESTENE SANCHEZ', 'CARRERA 14 CON CALLE 9 Y 10  CASA Nº9-10', '0276-3430213', '0416-6716710', 1, 1),
(534, '20617595', 'GENESIS VANESA', 'ORTEGA CRUZ ', 'VERADA 5 CASA Nº 8', '', '04247039822', 1, 1),
(535, '19769243', 'DAHYAN KATHERIN', 'DELGADO PARADA', 'CALLE 4 CASA Nº 2-195 SANTA TERESA', '', '0424-9294042', 1, 1),
(536, '12234929', 'CARLOS ESTEBAN', 'GRATEROL MATOS', 'VEREDA 6 CASA S/N ABEJAL DE PALMIRA', '', '', 1, 1),
(537, '21222112', 'JUAN CARLOS', 'OMAÑA VALERO', 'CALLE PRINCIPAL CASA Nº 6-17 BARRIO EL RIO', '0276-3480905', '', 1, 1),
(538, '19777663', 'BELKYS ELEANETH', 'SOLANO DURAN', 'CARRERA 31 Nº 61-90', '0276-3411240', '04124267572', 1, 1),
(539, '25463713', 'YENIFFER KARINA ', 'PATIÑO ZAMBRANO', 'CAPACHO NUEVO ', '0276-7881971', '04147519450', 1, 1),
(540, '18162357', 'JOHANA CROBANOSKY', 'HUERTA CUBIDES', 'BARRANCAS PTE.BAJA, AV. 2 CON CALLE 2', '0276-5995435', '0416-0740384', 1, 1),
(541, '18380307', 'FRANKLIN DE JESUS', 'FERRER RODRIGUEZ', 'LLANITO VIA CAPACHO KM 8 VEREDA EL LLANITO CASA S/N', '02467886856', '04247672997', 1, 1),
(542, '18564527', 'JACKSON FONTAINE', 'GARCIA WILCHEZ', '23 DE ENERO PARTE ALTA CALLE 11', '04169952122', '', 1, 1),
(543, '19134257', 'MANUEL ALEXANDER', 'JAIMES FLOREZ', 'PALO GORDO CALLE 5 CASA Nª4-02', '0276-3562108', '0414-7202016', 1, 1),
(544, '18565056', 'CARMEN KATHERINE', 'TORRES MALDONADO', 'URB. SAN JUAN BAUTISTA 3 LA MACHIRI', '0276-3485681', '0424-7369713', 1, 1),
(545, '11505651', 'HANS CARLOS', 'DELGADO MORENO', 'CALLE PRINCIPAL DE MONTE BELLO CASA Nº 0-127', '02769544976', '04247008272', 1, 1),
(546, '18989983', 'CARMEN LUSMARI', 'ROA SANCHEZ ', 'EL JUNCO ', '0276-4150732', '0424-7756050', 1, 1),
(547, '19599113', 'OMARY ANDREINA', 'RINCON BENACHI', 'VEREDA 24 CASA Nº 22', '0276-3568348', '0424-7831569', 1, 1),
(548, '18791418', 'MARIA ELENA', 'MONTENEGRO GARCIA', 'AV. PPAL PUEBLO NUEVO', '0276-3531274', '0424-7711747', 1, 1),
(549, '11497193', 'MARCO AURELIO', 'BECERRA RODRIGUEZ ', 'ALDEA LA BLANQUITA SECTOR SANTA ISABEL', '02765179091', '04166032277', 1, 1),
(550, '18791141', 'MAYORI JENIRE', 'MEDINA LOPEZ', 'CARRERA 31 Nº 61-104', '02763564626', '04169741086', 1, 1),
(551, '16959479', 'JOSE MIGUEL', 'BONILLA GARCIA', 'CALLE 3 Nº 16-25 RUIZ PINEDA', '0276-7621935', '0424-7424482', 1, 1),
(552, '16541317', 'MILAGROS SOLVEY', 'QUINTERO PERNIA ', 'AV. PPAL UNIDAD VECINAL CASA Nº 3-08', '0276-3476357', '0424-7311364', 1, 1),
(553, '20879939', 'JOSE ENMANUEL', 'VIVAS RICO', 'BARRIO URDANETA CALLE 3 ESQUINA CARRERA 0 APTO. 5', '04247543021', '04247522512', 1, 1),
(554, '25167280', 'KEVIN JOSE', 'NIÑO BAEZ', 'LA TINTA VIA GUAYABAL FRENTE AL MULATAL PORTON NEGRO', '', '04264676708', 1, 1),
(555, '15881759', 'YASDEY ADRIANA', 'MORA MARQUEZ', 'CARRERA 5 CASA Nº3-28A PALO GORDO', '04247849587', '04263729831', 1, 1),
(556, '27566377', 'DEDNNY GREGORY', 'HERNANDEZ VILLAMIZAR', 'SAN JOSECITO BARRIO LOS ANDES CALLE 6 VEREDA 3', '02763530363', '04169722082', 1, 1),
(557, '17107220', 'JAIBERTH ALBERTO', 'ZAMBRANO COLMENARES', 'Urbanizacion La Castra', '', '', 1, 1),
(558, '21418129', ' LUZANGEL', 'MOROS ALVAREZ', 'AVENIDA FERRERO TAMAYO CASA 16 ', '0276-3421279', '0424-7018872', 1, 1),
(559, '14417896', 'JUAN CARLOS', 'CARRIZO RODRIGUEZ', 'SAN CRISTOBAL', '', '', 1, 1),
(560, '16229432', 'ENGLER GONZALO', 'MONTOYA OSTOS', 'PASAJE YAGUAL CASA 12-10', '0276-6723242', '0416-2766859', 1, 1),
(561, '12227359', 'WILMER AGUSTIN', 'PEREZ DUQUE', 'CALLE 2 BIS CARRERA 7 CASA NRO. 3 -705 LOMAS BLANCAS', '', '', 1, 1),
(562, '19133896', 'JAVIER EDUARDO', 'PINEDA', 'CARRERA 9 CASA Nº8-28, ', '0276-3461734', '0426-7771476', 1, 1),
(563, '13973978', 'JORBAN ALEXANDER', 'CONTRERAS URBINA', 'TACHIRA', '', '', 1, 1),
(564, '13550279', 'JUAN ALBERTO', 'OCHOA ESCALANTE', 'AV PRINCIPAL BOCA DE CANEYES CASA NRO 7', '0276-3534407', '0426-5782216', 1, 1),
(565, '14785726', 'MAIYOLY DEL VALLE', 'DOMINGUEZ AUMAITRE', 'LA GUAYANA TORRES OASIS PISO 6 APTO. 6C', '02763423373', '04147075736', 1, 1),
(566, '20475845', 'ORLANDO DE JESUS', 'GARCIA GARCIA', 'LA CONCORDIA CASA Nº 2-41 CARRERA 12', '', '', 1, 1),
(567, '19776588', 'AURABY LISSETTE', 'SUAREZ SAYAGO', 'URB LA CASTRA BLOQUE 18 APTO 00-06', '0276-3483797', '0424-7384728', 1, 1),
(568, '4110866', 'JOSE ATILIO', 'CASTILLO ZAMBRANO', 'CALLE 15 CASA NRO. 16-69', '02763557622', '04247039164', 1, 1),
(569, '7924663', 'ALEXANDER ', 'COIZA MARTINEZ', 'SANTA TERESA CALLE 2 CON CARRERA 3 CASA Nº3-26', '0276-4248038', '0424-7809996', 1, 1),
(570, '16070634', 'MARYELIN LISNEET', 'ROMERO MARQUEZ', 'CALLE PRINCIPAL EDIF. BLOQUE 3 PISO 6 APTO 06-01 ', '04163714964', '04247074884', 1, 1),
(571, '14100834', 'THAIS CONSUELO', 'BUENAÑO ', 'F', '0', '0', 1, 1),
(572, '19359536', 'IRAN ALBERTO', 'MEDINA CHACON', 'S', 'S', 'S', 1, 1),
(573, '9226074', 'JORGE ENRIQUE', 'HIGUERA CARDENAS', 'SAN CRISTOBAL', '', '', 1, 1),
(574, '9129518', 'LAURA MARIBEL', 'ESCALANTE', 'SANTA TERESA', '', '', 1, 1),
(575, '24777727', 'ALEXIS JOSE', 'RAMIREZ MORA', 'SAN CRISTOBAL', '', '', 1, 1),
(576, '19134448', 'JOHAN ALFREDO', 'DELGADO DURAN', 'Calle 17 Casa Nº 4-52, Puerta El Sol', '0276-3435257', '0426-7774628', 1, 1),
(577, '25497076', 'LUZ ANDREINA', 'VILLAMIZAR CONDE', 'SECTOR SANTA RITA ESQUINA CARRERA 4 CON CALLE 2, CASA 2-3', '', '04247251224', 1, 1),
(578, '15760763', 'JOSE VIRGILIO', 'COLMENARES SANCHEZ', 'Calle Principal El Molino, Barrio Sucre', '0276-4212366', '0424-7483154', 1, 1),
(579, '12796292', 'MARIA MARGARITA', 'SILVA RIVERO', 'CALLE 1 Nº 2-155 B SANTA TERESA', '0276-3415224', '0426-5744645', 1, 1),
(580, '13792190', 'ROMEL ALBANO', 'RUIZ CALA', 'PASAJE ROMULO GALLEGOS CASA 6-19, 23 DE ENERO', '', '0424-1729207', 1, 1),
(581, '26014650', 'MARIA DE LOS ANGELES', 'QUINTERO BUITRAGO', 'VEREDA EL HIGUERON CASA SIN NUMERO HELECHALES PARTE BAJA', '', '0414-7079177', 1, 1),
(582, '27327314', 'YUSBELI YULIBETH', 'CONTRERAS GALINDO', 'Calle Sucre Casa Nº S-27, Barrancas Parte Alta', '0276-3918065', '0416-1749035', 1, 1),
(583, '26289805', 'ANALI MILENA', 'BECERRA MARIN', 'Av. La Bermeja con Calle Las Cumbas Qta Yrina', '0276-3554220', '0414-0079817', 1, 1),
(584, '9340154', 'ROMMEL ENRIQUE', 'CONTRERAS SOLANO', 'Carrera 2 Casa Nº 2 - 49', '0276-4155777', '0414-7331739', 1, 1),
(585, '14179283', 'MIGUEL ENRIQUE', 'PRECIADO RODRIGUEZ', 'Av. Principal Guayana Casa Nº 1-5', '', '0414-7364387', 1, 1),
(586, '17876188', 'AMERICA ALEXANDRA', 'SANCHEZ MARTINEZ', 'AV. 3 CON CALLE 15 VEREDA 1 CASA 67-09 LA VICTORIA PARTE ALTA', '0276-7622692', '0426-9747041', 1, 1),
(587, '24781641', 'GABRIELA COROMOTO', 'GIULIO PARRA', 'Arjona', '', '', 1, 1),
(588, '5668472', 'CARLOS ADELMO', 'MALDONADO DELGADO', 'SAN CRISTOBAL', '', '', 1, 1),
(589, '19768476', 'YURLEY', 'URIBE VILLAMIZAR', 'AV. PPAL CARRERA 4 CASA 1-1 BARRIO LAS FLORES', '0276-3475780', '0412-7638150', 1, 1),
(590, '28564376', 'NELSON', 'SANCHEZ', 'SAN CRISTOBAL', '', '', 1, 1),
(591, '14503511', 'OMAR', 'PEREZ', 'SAN CRISTOBAL', '', '', 1, 1),
(592, '26403488', 'JESUS ALFREDO', 'DELGADO SANCHEZ', 'SAN CRISTOBAL', '02763942358', '04247535946', 1, 1),
(593, '26767242', 'KARELYS', 'USECHE', 'SAN CRISTOBAL', '', '', 1, 1),
(594, '26069123', 'JESUS', 'USECHE', 'SAN CRISTOBAL', '', '', 1, 1),
(595, '10488641', 'RAMOELD', 'DUQUE', 'SAN CRISTOBAL', '', '', 1, 1),
(596, '19358884', 'CHARLY', 'GARCIA', 'SAN CRISTOBAL', '', '', 1, 1),
(597, '26934385', 'KELVIN', 'FLOREZ', 'SAN CRISTOBAL', '', '', 1, 1),
(598, '19359374', 'MARY', 'LATORRE', 'SAN CRISTOBAL', '', '', 1, 1),
(599, '17206274', 'LUSSIETH ALEXANDRA', 'GUERRERO ABRIL', 'PUEBLO NUEVO SECTOR POLIGONO DE TIRO CALLELOS SAUCES QUINTA GALA', '0276-3532319', '0414-7161038', 1, 1),
(600, '16124591', 'YALEYSY MARIA', 'GARCIA DE BOSCAN', 'AVENIDA PPAL DE PUEBO NUEVO,CONJUNTO RESIDENCIAL MONTECARLO', '02763536780', '04247469627', 1, 1),
(601, '26862189', 'JOSUE MANUEL', 'PADRON CONTRERAS', 'AV. LAS FLORES Y CALLE 1 BLOQUE 3 APTO 2-B UNIDAD VECINAL', '0276-3462120', '0426-1217664', 1, 1),
(602, '26702493', 'CLEYDIS ANGELY', 'QUINTERO RONDÓN', 'CALLE 2 VEREDA 1-63 SANTA TERESA', '', '0416-1328471', 1, 1),
(603, '15502279', 'ROGER ERNESTO', 'RODRIGUEZ HERNANDEZ', 'VEREDA 42 LOTE D CASA  Nº 2', '0414-4513423', '041-7677287', 1, 1),
(604, '9243118', 'SOLENI RUTH', 'BELTRAN CELIS', 'CALLE PRINCIPAL PARTE ALTA CASA Nº 88-62 CUESTA DEL TRAPICHE', '0276-3471318', '0414-7382805', 1, 1),
(605, '28195104', 'LEIDY SARAHY', 'HERNANDEZ MONSALVE', 'CALLE ELEAZAR LÓPEZ CONTRERAS CASA S/N SECTOR BELANDRIA', '', '', 1, 1),
(606, '19359007', 'STHEFANNY ROSELIA', 'MALDONADO ZAMBRANO', 'CALLE 9 CASA 175 ACEVITA', '0276-3489285', '0416-3769631', 1, 1),
(607, '24775529', 'LUIS MIGUEL', 'GUTIERREZ AYALA', 'CALLE 3 CASA Mº 6-A BARRIO 23 DE ENERO PARTE ALTA', '0276-6517878', '0412-2994107', 1, 1),
(608, '19134541', 'REDIN ANTONIO', 'MATTOS SUAREZ', 'VIA PRINCIPAL LA LAGUNA PARTE ALTA PARCELAMIENTO LA JUNISA CHALET Nº 1', '0276-230071', '0424-7694401', 1, 1),
(609, '19975570', 'CIRO ALFONSO', 'RUBIO TAPIA', 'CALLE PRINCIPAL Nº 1 CASA Nº 4', '0276-5160103', '0424-7730842', 1, 1),
(610, '19235519', 'JOSE LEANDRO', 'COLMENARES CARVAJAL', 'AV. ROTARIA CASA Nº 122', '0276-3480961', '0414-7238513', 1, 1),
(611, '26673076', 'OSCAR ALEXANDER', 'RODRIGUEZ MONTAÑEZ', 'CALLE 5 ENTRE CARRERAS 7 Y 8 CASA Nº 7-49 PATIECITOS', '', '0414-7399367', 1, 1),
(612, '25167710', 'YELIMAR', 'RUIZ CASTELLANOS', 'CALLE 14 ENTRE SEPTIMA Y CARRERA 6 CASA Nº 6-28', '', '0414-7279090', 1, 1),
(613, '27989233', 'SERGIO ANTONIO', 'SIERRA BALAGUERA', 'CARRETERA PPAL VÍA CAPACHO KM 5 CASA S/N SECTOR CURVA LOS CHUCHOS', '', '0424-7131203', 1, 1),
(614, '23152807', 'JAVIER RICARGO', 'VARGAS CAMACHO', 'CARRERA 10 CON CALLE 12 BARRIO MONSEÑOR BRICEÑO', '', '0426-3272736', 1, 1),
(615, '24784690', 'ESTEFANI YUSLEY', 'AVENDAÑO CÁRDENAS', 'CALLE 4 VEREDA 20 CASA Nº 4', '', '0426-1716608', 1, 1),
(616, '20426763', 'JUNIOR JOSÉ FILEMON', 'LAZARO AGELVIS', 'CALLE 7 MEDIA CUADRA DEL BANCO SOFITASA', '0412-7856076', '0424-7252531', 1, 1),
(617, '14504243', 'CARLOS JOSE', 'MONTOYA OSTOS', 'CARRERA 6 CASA # A-34 LAS VEGAS DE TÁRIBA', '', '0414-3761693', 1, 1),
(618, '10177630', 'JANETH STELLA', 'ARIAS ALJURE', 'VEGAS DE TARIBA, U CASA B3', '0276-3948942', '04247013935', 1, 1),
(619, '17169546', 'JEAN CARLOS', 'CHAVEZ DONCEL', 'SAN JUAN DE COLON', '0277/3754016', '0412/6484331', 1, 1),
(620, '24820254', 'MARIA GABRIELA', 'OMAÑA BACCA ', 'Palo Gordo Calle El Medio', '', '0424-7165791', 1, 1),
(621, '25889248', 'YAIRI ELIZABETH', 'SANCHEZ ROA', 'CALLE PPAL EL BORDO VEREDA SAN JOSE CASA 16-191E', '0276-7961406', '0412-7505200', 1, 1),
(622, '25889493', 'ANGEL YOEL ', 'GUERRERO PARADA ', 'MATA DE GUADUA VIA EL VALLE, CALLE LOS BUCARES', '', '0414-3742109', 1, 1),
(623, '26290498', 'FERNANDO', 'FERNANDEZ CONTRERAS ', 'SAN CRISTOBAL', '', '', 1, 1),
(624, '14605101', 'JOEL LEONARDO', 'VILLAMIZAR GONZALEZ', 'VIA PRINCIPAL DE LA MARICHI', '02763485824', '04247696363', 1, 1),
(625, '15501337', 'JOSEPH NEHOMAR', 'OVALLES CONTRERAS', 'PALMIRA LA LAGUNA CALLE BOLIVAR CASA 2-157', '02765994905', '04163727705', 1, 1),
(1215, '18564376', 'NELSON ANTONIO', 'SANCHEZ CEGARRA', 'SAN CRISTOBAL', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `indicador`
--

CREATE TABLE `indicador` (
  `ide_ind` int(6) NOT NULL,
  `des_ind` varchar(60) NOT NULL,
  `ide_tii` int(6) NOT NULL,
  `sta_ind` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `indicador`
--

INSERT INTO `indicador` (`ide_ind`, `des_ind`, `ide_tii`, `sta_ind`) VALUES
(1, 'Moto con Moto', 5, 1),
(2, 'Vehiculo con Vehiculo', 5, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingreso`
--

CREATE TABLE `ingreso` (
  `usu_ing` varchar(60) NOT NULL,
  `ipe_ing` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ingreso`
--

INSERT INTO `ingreso` (`usu_ing`, `ipe_ing`) VALUES
('FamiliaRP', '::1'),
('FamiliaRP', '::1'),
('FamiliaRP', '::1'),
('FamiliaRP', '::1'),
('FamiliaRP', '::1'),
('FamiliaRP', '::1'),
('FamiliaRP', '::1'),
('FamiliaRP', '::1'),
('FamiliaRP', '::1'),
('yilber_rodriguez', '::1'),
('yilber_rodriguez', '::1'),
('yilber_rodriguez', '::1'),
('yilber_rodriguez', '::1'),
('yilber_rodriguez', '::1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipio`
--

CREATE TABLE `municipio` (
  `ide_mun` int(6) NOT NULL,
  `ide_est` int(6) NOT NULL,
  `des_mun` varchar(60) NOT NULL,
  `sta_mun` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `municipio`
--

INSERT INTO `municipio` (`ide_mun`, `ide_est`, `des_mun`, `sta_mun`) VALUES
(10, 1, 'San Cristobal', 1),
(11, 1, 'Uribante', 1),
(12, 1, 'AndrÃ©s Bello', 1),
(13, 1, 'Antonio RÃ³mulo Costa', 1),
(14, 1, 'Ayacucho', 1),
(15, 1, 'BolÃ­var', 1),
(16, 1, 'CÃ¡rdenas', 1),
(17, 1, 'CÃ³rdoba', 1),
(18, 1, 'FernÃ¡ndez Feo', 1),
(19, 1, 'Francisco de Miranda', 1),
(20, 1, 'GarcÃ­a de Hevia', 1),
(21, 1, 'GuÃ¡simos', 1),
(22, 1, 'Capacho Nuevo', 1),
(23, 1, 'JÃ¡uregui', 1),
(24, 1, 'JosÃ© MarÃ­a Vargas', 1),
(25, 1, 'JunÃ­n', 1),
(26, 1, 'San Judas Tadeo', 1),
(27, 1, 'Capacho Viejo', 1),
(28, 1, 'Libertador', 1),
(29, 1, 'Lobatera', 1),
(30, 1, 'Michelena', 1),
(31, 1, 'Panamericano', 1),
(32, 1, 'Pedro MarÃ­a UreÃ±a', 1),
(33, 1, 'Rafael Urdaneta', 1),
(34, 1, 'Samuel DarÃ­o Maldonado', 1),
(35, 1, 'Seboruco', 1),
(36, 1, 'SimÃ³n RodrÃ­guez', 1),
(37, 1, 'Sucre', 1),
(38, 1, 'Torbes', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `organismo`
--

CREATE TABLE `organismo` (
  `ide_org` int(6) NOT NULL,
  `des_org` varchar(60) NOT NULL,
  `sta_org` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `organismo`
--

INSERT INTO `organismo` (`ide_org`, `des_org`, `sta_org`) VALUES
(1, 'Bomberos Municipal', 1),
(2, 'Guardia Nacional Bolivariana', 1),
(3, 'Policia Nacional', 1),
(4, 'Proteccion Civil Estadal', 1),
(5, 'Proteccion Civil Municipal', 1),
(6, 'Policia Estadal', 1),
(7, 'Imparques', 1),
(8, '911', 1),
(9, 'ODDI San Cristobal', 1),
(10, 'Personal', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `ide_pai` int(6) NOT NULL,
  `des_pai` varchar(120) NOT NULL,
  `sta_pai` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`ide_pai`, `des_pai`, `sta_pai`) VALUES
(1, 'Afganistán', 1),
(2, 'Albania', 1),
(3, 'Alemania', 1),
(4, 'Andorra', 1),
(5, 'Angola', 1),
(6, 'Antigua y Barbuda', 1),
(7, 'Arabia Saudita', 1),
(8, 'Argelia', 1),
(9, 'Argentina', 1),
(10, 'Armenia', 1),
(11, 'Australia', 1),
(12, 'Austria', 1),
(13, 'Azerbaiyán', 1),
(14, 'Bahamas', 1),
(15, 'Bangladés', 1),
(16, 'Barbados', 1),
(17, 'Baréin', 1),
(18, 'Bélgica', 1),
(19, 'Belice', 1),
(20, 'Benín', 1),
(21, 'Bielorrusia', 1),
(22, 'Birmania', 1),
(23, 'Bolivia', 1),
(24, 'Bosnia y Herzegovina', 1),
(25, 'Botsuana', 1),
(26, 'Brasil', 1),
(27, 'Brunéi', 1),
(28, 'Bulgaria', 1),
(29, 'Burkina Faso', 1),
(30, 'Burundi', 1),
(31, 'Bután', 1),
(32, 'Cabo Verde', 1),
(33, 'Camboya', 1),
(34, 'Camerún', 1),
(35, 'Canadá', 1),
(36, 'Catar', 1),
(37, 'Chad', 1),
(38, 'Chile', 1),
(39, 'China', 1),
(40, 'Chipre', 1),
(41, 'Ciudad del Vaticano', 1),
(42, 'Colombia', 1),
(43, 'Comoras', 1),
(44, 'Corea del Norte', 1),
(45, 'Corea del Sur', 1),
(46, 'Costa de Marfil', 1),
(47, 'Costa Rica', 1),
(48, 'Croacia', 1),
(49, 'Cuba', 1),
(50, 'Dinamarca', 1),
(51, 'Dominica', 1),
(52, 'Ecuador', 1),
(53, 'Egipto', 1),
(54, 'El Salvador', 1),
(55, 'Emiratos Árabes Unidos', 1),
(56, 'Eritrea', 1),
(57, 'Eslovaquia', 1),
(58, 'Eslovenia', 1),
(59, 'España', 1),
(60, 'Estados Unidos', 1),
(61, 'Estonia', 1),
(62, 'Etiopía', 1),
(63, 'Filipinas', 1),
(64, 'Finlandia', 1),
(65, 'Fiyi', 1),
(66, 'Francia', 1),
(67, 'Gabón', 1),
(68, 'Gambia', 1),
(69, 'Georgia', 1),
(70, 'Ghana', 1),
(71, 'Granada', 1),
(72, 'Grecia', 1),
(73, 'Guatemala', 1),
(74, 'Guyana', 1),
(75, 'Guinea', 1),
(76, 'Guinea ecuatorial', 1),
(77, 'Guinea-Bisáu', 1),
(78, 'Haití', 1),
(79, 'Honduras', 1),
(80, 'Hungría', 1),
(81, 'India', 1),
(82, 'Indonesia', 1),
(83, 'Irak', 1),
(84, 'Irán', 1),
(85, 'Irlanda', 1),
(86, 'Islandia', 1),
(87, 'Islas Marshall', 1),
(88, 'Islas Salomón', 1),
(89, 'Israel', 1),
(90, 'Italia', 1),
(91, 'Jamaica', 1),
(92, 'Japón', 1),
(93, 'Jordania', 1),
(94, 'Kazajistán', 1),
(95, 'Kenia', 1),
(96, 'Kirguistán', 1),
(97, 'Kiribati', 1),
(98, 'Kuwait', 1),
(99, 'Laos', 1),
(100, 'Lesoto', 1),
(101, 'Letonia', 1),
(102, 'Líbano', 1),
(103, 'Liberia', 1),
(104, 'Libia', 1),
(105, 'Liechtenstein', 1),
(106, 'Lituania', 1),
(107, 'Luxemburgo', 1),
(108, 'Macedonia del Norte', 1),
(109, 'Madagascar', 1),
(110, 'Malasia', 1),
(111, 'Malaui', 1),
(112, 'Maldivas', 1),
(113, 'Malí', 1),
(114, 'Malta', 1),
(115, 'Marruecos', 1),
(116, 'Mauricio', 1),
(117, 'Mauritania', 1),
(118, 'México', 1),
(119, 'Micronesia', 1),
(120, 'Moldavia', 1),
(121, 'Mónaco', 1),
(122, 'Mongolia', 1),
(123, 'Montenegro', 1),
(124, 'Mozambique', 1),
(125, 'Namibia', 1),
(126, 'Nauru', 1),
(127, 'Nepal', 1),
(128, 'Nicaragua', 1),
(129, 'Níger', 1),
(130, 'Nigeria', 1),
(131, 'Noruega', 1),
(132, 'Nueva Zelanda', 1),
(133, 'Omán', 1),
(134, 'Países Bajos', 1),
(135, 'Pakistán', 1),
(136, 'Palaos', 1),
(137, 'Panamá', 1),
(138, 'Papúa Nueva Guinea', 1),
(139, 'Paraguay', 1),
(140, 'Perú', 1),
(141, 'Polonia', 1),
(142, 'Portugal', 1),
(143, 'Reino Unido', 1),
(144, 'República Centroafricana', 1),
(145, 'República Checa', 1),
(146, 'República del Congo', 1),
(147, 'República Democrática del Congo', 1),
(148, 'República Dominicana', 1),
(149, 'Ruanda', 1),
(150, 'Rumanía', 1),
(151, 'Rusia', 1),
(152, 'Samoa', 1),
(153, 'San Cristóbal y Nieves', 1),
(154, 'San Marino', 1),
(155, 'San Vicente y las Granadinas', 1),
(156, 'Santa Lucía', 1),
(157, 'Santo Tomé y Príncipe', 1),
(158, 'Senegal', 1),
(159, 'Serbia', 1),
(160, 'Seychelles', 1),
(161, 'Sierra Leona', 1),
(162, 'Singapur', 1),
(163, 'Siria', 1),
(164, 'Somalia', 1),
(165, 'Sri Lanka', 1),
(166, 'Suazilandia', 1),
(167, 'Sudáfrica', 1),
(168, 'Sudán', 1),
(169, 'Sudán del Sur', 1),
(170, 'Suecia', 1),
(171, 'Suiza', 1),
(172, 'Surinam', 1),
(173, 'Tailandia', 1),
(174, 'Tanzania', 1),
(175, 'Tayikistán', 1),
(176, 'Timor Oriental', 1),
(177, 'Togo', 1),
(178, 'Tonga', 1),
(179, 'Trinidad y Tobago', 1),
(180, 'Túnez', 1),
(181, 'Turkmenistán', 1),
(182, 'Turquía', 1),
(183, 'Tuvalu', 1),
(184, 'Ucrania', 1),
(185, 'Uganda', 1),
(186, 'Uruguay', 1),
(187, 'Uzbekistán', 1),
(188, 'Vanuatu', 1),
(189, 'Venezuela', 1),
(190, 'Vietnam', 1),
(191, 'Yemen', 1),
(192, 'Yibuti', 1),
(193, 'Zambia', 1),
(194, 'Zimbabue', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parroquia`
--

CREATE TABLE `parroquia` (
  `ide_par` int(6) NOT NULL,
  `ide_mun` int(6) NOT NULL,
  `des_par` varchar(60) NOT NULL,
  `sta_par` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `parroquia`
--

INSERT INTO `parroquia` (`ide_par`, `ide_mun`, `des_par`, `sta_par`) VALUES
(7, 12, 'Andres Bello', 1),
(9, 13, 'Antonio RÃ³mulo Costa', 1),
(10, 14, 'Ayacucho', 1),
(11, 14, 'Rivas Berti', 1),
(12, 14, 'San Pedro del RÃ­o', 1),
(13, 15, 'BolÃ­var', 1),
(14, 15, 'Palotal', 1),
(15, 15, 'General Juan Vicente GÃ³mez', 1),
(16, 15, 'IsaÃ­as Medina Angarita', 1),
(17, 16, 'CÃ¡rdenas C', 1),
(18, 16, 'Amenodoro Rangel Lamus', 1),
(19, 16, 'La Florida', 1),
(20, 17, 'CÃ³rdoba', 1),
(21, 18, 'FernÃ¡ndez Feo', 1),
(22, 18, 'Alberto Adriani', 1),
(23, 18, 'Santo Domingo', 1),
(24, 19, 'Francisco de Miranda', 1),
(25, 20, 'GarcÃ­a de Hevia', 1),
(26, 20, 'Boca de Grita', 1),
(27, 20, 'JosÃ© Antonio PÃ¡ez', 1),
(28, 21, 'GuÃ¡simos', 1),
(29, 22, 'Independencia', 1),
(30, 22, 'Juan GermÃ¡n Roscio', 1),
(31, 22, 'RomÃ¡n CÃ¡rdenas', 1),
(32, 23, 'JÃ¡uregui', 1),
(33, 23, 'Emilio Constantino Guerrero', 1),
(34, 23, 'MonseÃ±or Miguel Antonio Salas', 1),
(35, 24, 'JosÃ© MarÃ­a Vargas', 1),
(36, 25, 'JunÃ­n', 1),
(37, 25, 'La PetrÃ³lea', 1),
(38, 25, 'QuinimarÃ­', 1),
(39, 25, 'BramÃ³n', 1),
(40, 27, 'Libertad', 1),
(41, 27, 'Cipriano Castro', 1),
(42, 27, 'Manuel Felipe Rugeles', 1),
(43, 28, 'Capital Libertador', 1),
(44, 28, 'Doradas', 1),
(45, 28, 'Emeterio Ochoa', 1),
(46, 28, 'San JoaquÃ­n de Navay', 1),
(47, 29, 'Lobatera', 1),
(48, 29, 'ConstituciÃ³n', 1),
(49, 30, 'Michelena', 1),
(50, 31, 'Panamericano', 1),
(51, 31, 'La Palmita', 1),
(52, 32, 'Pedro MarÃ­a UreÃ±a', 1),
(53, 32, 'Nueva Arcadia', 1),
(54, 33, 'Delicias', 1),
(55, 33, 'Pecaya', 1),
(56, 34, 'Samuel DarÃ­o Maldonado', 1),
(57, 34, 'BoconÃ³', 1),
(58, 34, 'HernÃ¡ndez', 1),
(59, 10, 'La Concordia', 1),
(60, 10, 'San Juan Bautista', 1),
(61, 10, 'Pedro MarÃ­a Morantes', 1),
(62, 10, 'San SebastiÃ¡n', 1),
(63, 10, 'Dr. Francisco Romero Lobo', 1),
(64, 35, 'Seboruco', 1),
(65, 36, 'SimÃ³n RodrÃ­guez', 1),
(66, 37, 'Sucre', 1),
(67, 37, 'Eleazar LÃ³pez Contreras', 1),
(68, 37, 'San Pablo', 1),
(69, 38, 'Torbes', 1),
(70, 11, 'Uribante', 1),
(71, 11, 'CÃ¡rdenas U', 1),
(72, 11, 'Juan Pablo PeÃ±alosa', 1),
(73, 11, 'PotosÃ­', 1),
(74, 26, 'San Judas Tadeo', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE `personal` (
  `ide_per` int(6) NOT NULL,
  `ced_per` varchar(14) NOT NULL,
  `nom_per` varchar(60) NOT NULL,
  `ape_per` varchar(60) NOT NULL,
  `dir_per` text NOT NULL,
  `tel_per` varchar(30) NOT NULL,
  `ide_dep` int(6) NOT NULL,
  `sta_per` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `software`
--

CREATE TABLE `software` (
  `ide_sof` int(6) NOT NULL,
  `des_sof` varchar(60) NOT NULL,
  `log_sof` text NOT NULL,
  `sta_sof` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_indicador`
--

CREATE TABLE `tipo_indicador` (
  `ide_tii` int(6) NOT NULL,
  `des_tii` varchar(60) NOT NULL,
  `sta_tii` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_indicador`
--

INSERT INTO `tipo_indicador` (`ide_tii`, `des_tii`, `sta_tii`) VALUES
(5, 'Hecho Vial', 1),
(6, 'Hecho Terrestre', 1),
(7, 'Atencion Local', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_unidad`
--

CREATE TABLE `tipo_unidad` (
  `ide_tip` int(6) NOT NULL,
  `des_tip` varchar(60) NOT NULL,
  `sta_tip` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_unidad`
--

INSERT INTO `tipo_unidad` (`ide_tip`, `des_tip`, `sta_tip`) VALUES
(1, 'Ambulancias', 1),
(2, 'Cisterna', 1),
(3, 'Motos', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidad`
--

CREATE TABLE `unidad` (
  `ide_uni` int(6) NOT NULL,
  `ide_tip` int(6) NOT NULL,
  `des_uni` varchar(60) NOT NULL,
  `sta_uni` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `unidad`
--

INSERT INTO `unidad` (`ide_uni`, `ide_tip`, `des_uni`, `sta_uni`) VALUES
(1, 2, 'A-18', 2),
(2, 1, 'A-38', 1),
(3, 2, 'R-5', 1),
(4, 1, 'A-45', 1),
(5, 3, 'M-5', 1),
(6, 2, 'D-26', 1),
(7, 1, 'A-621', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `ide_usu` int(6) NOT NULL,
  `nom_usu` varchar(30) NOT NULL,
  `cla_usu` varchar(30) NOT NULL,
  `niv_usu` int(1) NOT NULL,
  `sta_usu` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`ide_usu`, `nom_usu`, `cla_usu`, `niv_usu`, `sta_usu`) VALUES
(1, 'yilber_rodriguez', '123', 1, 1),
(2, 'Monica_Pacheco', '123', 2, 1),
(5, 'FamiliaRp', '123', 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`ide_car`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`ide_dep`);

--
-- Indices de la tabla `dependencia`
--
ALTER TABLE `dependencia`
  ADD PRIMARY KEY (`ide_dpe`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`ide_est`);

--
-- Indices de la tabla `funcionarios`
--
ALTER TABLE `funcionarios`
  ADD PRIMARY KEY (`ide_fun`),
  ADD UNIQUE KEY `ced_fun` (`ced_fun`);

--
-- Indices de la tabla `indicador`
--
ALTER TABLE `indicador`
  ADD PRIMARY KEY (`ide_ind`);

--
-- Indices de la tabla `municipio`
--
ALTER TABLE `municipio`
  ADD PRIMARY KEY (`ide_mun`);

--
-- Indices de la tabla `organismo`
--
ALTER TABLE `organismo`
  ADD UNIQUE KEY `ide` (`ide_org`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`ide_pai`);

--
-- Indices de la tabla `parroquia`
--
ALTER TABLE `parroquia`
  ADD PRIMARY KEY (`ide_par`);

--
-- Indices de la tabla `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`ide_per`);

--
-- Indices de la tabla `software`
--
ALTER TABLE `software`
  ADD PRIMARY KEY (`ide_sof`);

--
-- Indices de la tabla `tipo_indicador`
--
ALTER TABLE `tipo_indicador`
  ADD PRIMARY KEY (`ide_tii`);

--
-- Indices de la tabla `tipo_unidad`
--
ALTER TABLE `tipo_unidad`
  ADD PRIMARY KEY (`ide_tip`);

--
-- Indices de la tabla `unidad`
--
ALTER TABLE `unidad`
  ADD PRIMARY KEY (`ide_uni`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`ide_usu`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `ide_car` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `departamento`
--
ALTER TABLE `departamento`
  MODIFY `ide_dep` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `dependencia`
--
ALTER TABLE `dependencia`
  MODIFY `ide_dpe` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `ide_est` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `funcionarios`
--
ALTER TABLE `funcionarios`
  MODIFY `ide_fun` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1216;

--
-- AUTO_INCREMENT de la tabla `indicador`
--
ALTER TABLE `indicador`
  MODIFY `ide_ind` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `municipio`
--
ALTER TABLE `municipio`
  MODIFY `ide_mun` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `organismo`
--
ALTER TABLE `organismo`
  MODIFY `ide_org` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `ide_pai` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;

--
-- AUTO_INCREMENT de la tabla `parroquia`
--
ALTER TABLE `parroquia`
  MODIFY `ide_par` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT de la tabla `personal`
--
ALTER TABLE `personal`
  MODIFY `ide_per` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `software`
--
ALTER TABLE `software`
  MODIFY `ide_sof` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_indicador`
--
ALTER TABLE `tipo_indicador`
  MODIFY `ide_tii` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tipo_unidad`
--
ALTER TABLE `tipo_unidad`
  MODIFY `ide_tip` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `unidad`
--
ALTER TABLE `unidad`
  MODIFY `ide_uni` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `ide_usu` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
